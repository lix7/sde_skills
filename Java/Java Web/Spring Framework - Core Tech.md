# 参考

- https://docs.spring.io/spring-framework/docs/current/spring-framework-reference/core.html

# IoC Container

## IoC容器（IoC Container）

`org.springframework.context.ApplicationContext`代表IoC容器，负责实例化、装配和管理bean。容器通过读取配置元数据得到相关信息。

依赖注入（Dependency Injection）是Spring框架实现控制反转（Inversion of Control）的一种方式

`org.springframework.beans`和`org.springframework.context`是Spring实现IoC的基础。BeanFactory接口提供了最基础的工厂接口，ApplicationContext是BeanFactory的子接口，在其之上提供了更多J2EE的功能：AOP、i18n、事件机制、更多的应用层ApplicationContext实现（例如WebApplicationContext）

配置元信息可以通过XML、Java注解、Java代码的方式给出，它声明了对象（bean）和对象间的依赖关系
- [lix]个人认为注解的方式比较方便

### 配置元信息（Configuration Metadata）

本文使用XML来展示IoC的相关样例，但越来越多的Spring项目已经使用注解来配置容器

如下XML文件配置了一个bean

```xml
<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xsi:schemaLocation="http://www.springframework.org/schema/beans
        http://www.springframework.org/schema/beans/spring-beans.xsd">

    <bean id="bike" class="com.lix7.Bike">
        <!-- 定义了名为bike，类型为Bike的一个bean -->
    </bean>

    <!-- more bean definitions go here -->

</beans>
```

### 实例化容器

实例化容器很简单，就是实例化一个ApplicationContext的实现即可。

```java
ApplicationContext context = new ClassPathXmlApplicationContext("services.xml", "daos.xml");
```

services.xml文件如下

```XML
<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xsi:schemaLocation="http://www.springframework.org/schema/beans
        http://www.springframework.org/schema/beans/spring-beans.xsd">

    <!-- services -->

    <bean id="petStore" class="org.springframework.samples.jpetstore.services.PetStoreServiceImpl">
        <property name="accountDao" ref="accountDao"/>
        <property name="itemDao" ref="itemDao"/>
        <!-- additional collaborators and configuration for this bean go here -->
    </bean>

    <!-- more bean definitions for services go here -->

</beans>
```

daos.xml文件如下

```xml
<?xml version="1.0" encoding="UTF-8"?>
<beans xmlns="http://www.springframework.org/schema/beans"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xsi:schemaLocation="http://www.springframework.org/schema/beans
        http://www.springframework.org/schema/beans/spring-beans.xsd">

    <bean id="accountDao"
        class="org.springframework.samples.jpetstore.dao.jpa.JpaAccountDao">
        <!-- additional collaborators and configuration for this bean go here -->
    </bean>

    <bean id="itemDao" class="org.springframework.samples.jpetstore.dao.jpa.JpaItemDao">
        <!-- additional collaborators and configuration for this bean go here -->
    </bean>

    <!-- more bean definitions for data access objects go here -->

</beans>
```

service.xml文件中声明的bean中的`<property name="accountDao" ref="accountDao"/>`代表该bean拥有一个名为accountDao的成员，该成员的引用为accountDao这个bean（注意区分成员名与成员bean实例），这个bean在daos.xml文件中定义了。

### 组合XML配置文件

通常每一个配置文件代表了一类特定的业务逻辑或者模块，在配置文件过多的时候将他们组合起来会简洁的多，例如如下配置文件

```xml
<beans>
    <import resource="services.xml"/>
    <import resource="resources/messageSource.xml"/>
    <import resource="/resources/themeSource.xml"/>

    <bean id="bean1" class="..."/>
    <bean id="bean2" class="..."/>
</beans>
```

注意，所有的路径都是相对路径，对于`/resources/themeSource.xml`，Spring会忽略最开始的反斜杠，将该路径作为当前路径、resource或classpath的子路径看待

### 使用容器

如下代码展示了如何从容器中取出一个bean

```java
// create and configure beans
ApplicationContext context = new ClassPathXmlApplicationContext("services.xml", "daos.xml");

// retrieve configured instance
PetStoreService service = context.getBean("petStore", PetStoreService.class);

// use configured instance
List<String> userList = service.getUsernameList();	
```

需要注意的是，getBean中有参数PetSoreService.class参数，作为强制类型转换的个根据，该Bean的实际类型可以从配置文件中发现是PetSoreServiceImpl，这体现了工厂的优势，即可以通过不同的配置文件为同一个bean指定不同的实现。

## Bean

在Spring中，应用程序骨干（backbone）中的对象由IoC容器管理，这些对象称为**bean**

- bean是一个由IoC容器负责实例化、装配和管理的对象
- bean及其之间的依赖关系都写在容器所使用的配置元数据（Configuration Metadata）中

bean一般对应着实际应用中的Service Layer、data access objects（DAOs）、表示层对象、基础对象（持久层工厂）等。

通常不会将细粒度域对象配置为Bean，这类对象的创建是DAO和业务逻辑层的职责。

除了通过配置定义bean，还可以将自定义对象注册到spring中，但很不推荐！自定义注册很可能会导致容器发生同步异常。

### 属性

bean在容器内存储为BeanDefinition对象，该对象包含了一系列元信息，例如类名、Bean名称、作用域（scope）、构建参数、成员bean（依赖）、autowiring模式、懒加载模式、初始化方法、销毁方法，这些都会在随后做详细解释。

#### Bean命名，id or name

一个Bean可以有一个或多个名字，可以通过id或name字段指定。如果不显示的给出名字，则Spring会为其生成一个名字，这种不给定名字的行为一般用于inner beans或autowiring

### 依赖，dependency

#### 依赖注入（Dependency Injection）

##### 基于构造方法的DI

在参数类型可以隐式确定的时候可以不指定参数类型、顺序等信息，Spring会自动推导参数类型，例如下述示例：

```java
package x.y;

public class Foo {

    public Foo(Bar bar, Baz baz) {
        // ...
    }
}
```

```xml
<beans>
    <bean id="foo" class="x.y.Foo">
        <constructor-arg ref="bar"/>
        <constructor-arg ref="baz"/>
    </bean>

    <bean id="bar" class="x.y.Bar"/>

    <bean id="baz" class="x.y.Baz"/>
</beans>
```

在不能隐式确定参数匹配时，需要显示的指定，具体有如下几种方法

- 通过类型匹配

  ```XML
  <bean id="exampleBean" class="examples.ExampleBean">
      <constructor-arg type="int" value="7500000"/>
      <constructor-arg type="java.lang.String" value="42"/>
  </bean>
  ```

- 通过索引匹配，需要注意的是索引从0开始

  ```java
  <bean id="exampleBean" class="examples.ExampleBean">
      <constructor-arg index="0" value="7500000"/>
      <constructor-arg index="1" value="42"/>
  </bean>
  ```

- 通过参数名称匹配

  ```XML
  <bean id="exampleBean" class="examples.ExampleBean">
      <constructor-arg name="years" value="7500000"/>
      <constructor-arg name="ultimateAnswer" value="42"/>
  </bean>
  ```

  注意，如果要使该方法生效，需要在编译时打开debug开关，使得Spring可以查看构造方法的参数表。或者使用@ConstructorProperties这一JDK注解显示声明构造函数参数名称，示例如下：

  ```java
  package examples;
  
  public class ExampleBean {
  
      // Fields omitted
  
      @ConstructorProperties({"years", "ultimateAnswer"})
      public ExampleBean(int years, String ultimateAnswer) {
          this.years = years;
          this.ultimateAnswer = ultimateAnswer;
      }
  }
  ```

##### 基于setter的DI

Spring通过setter方法对bean的依赖进行赋值

```xml
<bean id="exampleBean" class="examples.ExampleBean">
    <!-- setter injection using the nested ref element -->
    <property name="beanOne">
        <ref bean="anotherExampleBean"/>
    </property>

    <!-- setter injection using the neater ref attribute -->
    <property name="beanTwo" ref="yetAnotherBean"/>
    <property name="integerProperty" value="1"/>
</bean>

<bean id="anotherExampleBean" class="examples.AnotherBean"/>
<bean id="yetAnotherBean" class="examples.YetAnotherBean"/>
```

```java
public class ExampleBean {

    private AnotherBean beanOne;

    private YetAnotherBean beanTwo;

    private int i;

    public void setBeanOne(AnotherBean beanOne) {
        this.beanOne = beanOne;
    }

    public void setBeanTwo(YetAnotherBean beanTwo) {
        this.beanTwo = beanTwo;
    }

    public void setIntegerProperty(int i) {
        this.i = i;
    }
}
```

##### 官方建议 关于construct-based or setter-based

对于强制必需的属性，使用构造方法；对于可选的属性，使用setter方法

#### p-namespace

property配置可以使用p-namespace简写，示例如下，注意原始类型（包括String）和引用类型的不同写法

```XML
<beans xmlns="http://www.springframework.org/schema/beans"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xmlns:p="http://www.springframework.org/schema/p"
    xsi:schemaLocation="http://www.springframework.org/schema/beans
        http://www.springframework.org/schema/beans/spring-beans.xsd">

    <bean name="john-classic" class="com.example.Person">
        <property name="name" value="John Doe"/>
        <property name="spouse" ref="jane"/>
    </bean>

    <!-- 注意引用类型后的“-ref”标志 -->
    <bean name="john-modern"
        class="com.example.Person"
        p:name="John Doe"
        p:spouse-ref="jane"/>

    <bean name="jane" class="com.example.Person">
        <property name="name" value="Jane Doe"/>
    </bean>
</beans>
```

#### c-namespace

与p-namespace类似，但是是用于构造方法参数的，示例如下：

```XML
<beans xmlns="http://www.springframework.org/schema/beans"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xmlns:c="http://www.springframework.org/schema/c"
    xsi:schemaLocation="http://www.springframework.org/schema/beans
        http://www.springframework.org/schema/beans/spring-beans.xsd">

    <bean id="bar" class="x.y.Bar"/>
    <bean id="baz" class="x.y.Baz"/>

    <!-- traditional declaration -->
    <bean id="foo" class="x.y.Foo">
        <constructor-arg ref="bar"/>
        <constructor-arg ref="baz"/>
        <constructor-arg value="foo@bar.com"/>
    </bean>

    <!-- c-namespace declaration -->
    <bean id="foo" class="x.y.Foo" c:bar-ref="bar" c:baz-ref="baz" c:email="foo@bar.com"/>
    
    <!-- c-namespace index declaration -->
    <!-- 不能使用name时的index版本 -->
    <!-- 由于XML语法限制，属性名不能以数字开头，所以index前要加下划线 -->
<bean id="foo" class="x.y.Foo" c:_0-ref="bar" c:_1-ref="baz"/>

</beans>
```

#### inner bean

#### Collections Bean

支持map、list、set、props

#### 空字符串和null

使用`“”`设置空字符串，使用`<null/>`设置null

#### depends-on

#### 懒加载beans

通过设置bean的`lazy-init`来声明是否需要懒加载该bean，该属性只对单例的bean生效

#### Autowiring

四种模式如下：

| Mode        | 说明                                                         |
| ----------- | ------------------------------------------------------------ |
| no          | 默认行为，不指定autowiring，需要显式使用ref                  |
| byName      | 通过寻找匹配name的bean来连线                                 |
| byType      | 通过寻找匹配type的bean来连线，如果存在多个type匹配的bean则抛出异常；如果不存在则不做任何操作 |
| constructor | 类似于byType，但是是针对构造方法中匹配的参数                 |

