# 引言

这篇文章主要涉及的是 select / poll / epoll / BIO / NIO / AIO ，旨在理清讲清同步、异步、阻塞、非阻塞 IO 的区别与其实现（UNIX 、Java）。

我只是一个学 Java 的，为什么我要把这么多东西放在一起？因为 Java IO 本质上是对操作系统的 IO 的封装。我曾无数次的想去直接学习 Java IO 但是放弃了，认为其概念过于抽象，并不能理解其原理。这样学习，就算最后知道了 API 怎么用，也只是邯郸学步。

# Ref

- [Linux IO模式及 select、poll、epoll详解](https://segmentfault.com/a/1190000003063859#articleHeader15)
- [Async IO on Linux: select, poll, and epoll](https://jvns.ca/blog/2017/06/03/async-io-on-linux--select--poll--and-epoll/)
- [Netty序章之BIO NIO AIO演变](https://segmentfault.com/a/1190000012976683)
- [Linux Programmer's Manual  SELECT(2)](http://man7.org/linux/man-pages/man2/select.2.html)
- [Linux Programmer's Manual  POLL(2)](http://man7.org/linux/man-pages/man2/poll.2.html)
- [Linux Programmer's Manual  EPOLL(7)](http://man7.org/linux/man-pages/man7/epoll.7.html)

# SELECT / POLL / EPOLL

这三个**系统调用**是系统层面的，所以放在一起讲。实质上 SELECT 和 POLL 行为基本一致，EPOLL 是一个行为不同的改进版本。下面的英文资料和源码来源于 Linux Programmer's Manual，手册本身会完整的描述函数行为，但此处我只摘出部分我认为对于理解这三个系统调用的关键描述。为了避免翻译过程出出现歧义或信息丢失，我不会翻译任何原文为英文的内容，文章中插入的中文是我自己的一些理解。

## SELECT - synchronous I/O multiplexing

`select`函数声明如下，传入参数指明了要读取的文件描述符、要写入的文件描述符、异常、超时间隔。

```c
int select(int nfds, fd_set *readfds, fd_set *writefds,
    		fd_set *exceptfds, struct timeval *timeout);
```

`select()` and `pselect()` allow a program to monitor multiple file descriptors, waiting until one or more of the file descriptors become "ready" for some class of I/O operation (e.g., input possible).  A file descriptor is considered ready if it is possible to perform a corresponding I/O operation (e.g., read, or a sufficiently small write) **without blocking**.

`select()` can monitor only file descriptors numbers that are less than `FD_SETSIZE`; `poll` does not have this limitation.  See BUGS.

`select` 因为使用数组保存文件描述符的，所以最大描述符数量受限于`FD_SETSIZE`这个宏。可以通过重新编译的方法修改该值大小，但意义不大。过大的值会导致`select`轮询变慢，影响性能。`poll`通过将存储文件描述符的方式改为链表解决了这个问题，但实际上过多的文件描述符仍会导致性能下降。这一点也是是 `select` 与 `poll` 最大的区别。

Three independent sets of file descriptors are watched. The file descriptors listed in `readfds` will be watched to see if characters become available for reading (more precisely, to see if a read **will not block**; in particular, a file descriptor is also ready on end-of-file).  The file descriptors in `writefds` will be watched to see if space is available for write (though a large write may still block). The file descriptors in `exceptfds` will be watched for exceptional conditions.  (For examples of some exceptional conditions, see the discussion of POLLPRI in poll.)

The `timeout` argument specifies the interval that `select()` should **block** waiting for a file descriptor to become ready. The call will block until either:

   *  a file descriptor becomes ready;

   *  the call is interrupted by a signal handler; or

   *  the timeout expires.

从描述中不难看出，`select`仍然会阻塞进程，这不过这个阻塞不是 IO 导致的，而是`select`在等待 IO Ready 。 

![](.UNIX%20IO%20and%20Java%20IO.asset/2019-07-17-17-18-13-UNIX%20IO%20and%20Java%20IO.png)

# POLL - wait for some event on a file descriptor

`POLL`函数声明如下：

```c
struct pollfd {
    int   fd;         /* file descriptor */
    short events;     /* requested events */
    short revents;    /* returned events */
};

int poll(struct pollfd *fds, nfds_t nfds, int timeout);
```

`poll()` performs a similar task to `select`: it waits for one of a set of file descriptors to become ready to perform I/O.

The set of file descriptors to be monitored is specified in the `fds` argument, which is an array of `pollfd` structures. The caller should specify the number of items in the `fds` array in `nfds`.

If none of the events requested (and no error) has occurred for any of the file descriptors, then `poll()` **blocks** until one of the events occurs.

The `timeout` argument specifies the number of milliseconds that `poll()` should **block** waiting for a file descriptor to become ready. The call will **block** until either:

*  a file descriptor becomes ready;

*  the call is interrupted by a signal handler; or

*  the timeout expires.

# EPOLL - I/O event notification facility

The `epoll` API performs a similar task to `poll`: monitoring multiple file descriptors to see if I/O is possible on any of them. The `epoll` API can be used either as an `edge-triggered` or a `level-triggered` interface and **scales well to large numbers of watched file descriptors**.

The central concept of the epoll API is the **epoll instance**, an in-kernel data structure which, from a user-space perspective, can be considered as a container for two lists:

* **The interest list** (sometimes also called the epoll set): the set of file descriptors that the process has **registered** an interest in monitoring.

* **The ready list**: the set of file descriptors that are "ready" for I/O.  The ready list is a subset of (or, more precisely, a set of references to) the file descriptors in the interest list that is dynamically populated by the kernel as a result of I/O activity on those file descriptors.

The following system calls are provided to create and manage an epoll instance:

*  `epoll_create` creates a new epoll instance and returns a file descriptor referring to that instance.

*  Interest in particular file descriptors is then **registered** via `epoll_ctl`, which adds items to the interest list of the epoll instance.

*  `epoll_wait` waits for I/O events, **blocking** the calling thread if no events are currently available.  (This system call can be thought of as fetching items from the ready list of the epoll instance.)

## Level-triggered and edge-triggered