---
blog: true
title: Java 面试相关
---

- [Java](#java)
	- [基础](#%e5%9f%ba%e7%a1%80)
		- [Java的四大特性？](#java%e7%9a%84%e5%9b%9b%e5%a4%a7%e7%89%b9%e6%80%a7)
		- [`==`和hashCode和equals？为什么要重写equals方法？](#%e5%92%8chashcode%e5%92%8cequals%e4%b8%ba%e4%bb%80%e4%b9%88%e8%a6%81%e9%87%8d%e5%86%99equals%e6%96%b9%e6%b3%95)
		- [HashMap中使用可变对象作为key需要注意什么？](#hashmap%e4%b8%ad%e4%bd%bf%e7%94%a8%e5%8f%af%e5%8f%98%e5%af%b9%e8%b1%a1%e4%bd%9c%e4%b8%bakey%e9%9c%80%e8%a6%81%e6%b3%a8%e6%84%8f%e4%bb%80%e4%b9%88)
		- [Java的集合类整体接口和抽象类设计？](#java%e7%9a%84%e9%9b%86%e5%90%88%e7%b1%bb%e6%95%b4%e4%bd%93%e6%8e%a5%e5%8f%a3%e5%92%8c%e6%8a%bd%e8%b1%a1%e7%b1%bb%e8%ae%be%e8%ae%a1)
		- [ArrayList、LinkedList基础空间、扩展方案？](#arraylistlinkedlist%e5%9f%ba%e7%a1%80%e7%a9%ba%e9%97%b4%e6%89%a9%e5%b1%95%e6%96%b9%e6%a1%88)
		- [HashMap实现？添加过程？rehash过程？Treeify？加载因子？初始容量？resize规则？Hash策略？为什么容量为2的n次幂？put策略？](#hashmap%e5%ae%9e%e7%8e%b0%e6%b7%bb%e5%8a%a0%e8%bf%87%e7%a8%8brehash%e8%bf%87%e7%a8%8btreeify%e5%8a%a0%e8%bd%bd%e5%9b%a0%e5%ad%90%e5%88%9d%e5%a7%8b%e5%ae%b9%e9%87%8fresize%e8%a7%84%e5%88%99hash%e7%ad%96%e7%95%a5%e4%b8%ba%e4%bb%80%e4%b9%88%e5%ae%b9%e9%87%8f%e4%b8%ba2%e7%9a%84n%e6%ac%a1%e5%b9%82put%e7%ad%96%e7%95%a5)
		- [1.7和1.8的HashMap区别？](#17%e5%92%8c18%e7%9a%84hashmap%e5%8c%ba%e5%88%ab)
		- [HashMap与HashTable区别？](#hashmap%e4%b8%8ehashtable%e5%8c%ba%e5%88%ab)
		- [Java中的异常机制？](#java%e4%b8%ad%e7%9a%84%e5%bc%82%e5%b8%b8%e6%9c%ba%e5%88%b6)
		- [BIO/NIO/AIO？todo](#bionioaiotodo)
		- [Java8的函数式接口？](#java8%e7%9a%84%e5%87%bd%e6%95%b0%e5%bc%8f%e6%8e%a5%e5%8f%a3)
		- [Java9的新特性？todo](#java9%e7%9a%84%e6%96%b0%e7%89%b9%e6%80%a7todo)
		- [Java10新特性？todo](#java10%e6%96%b0%e7%89%b9%e6%80%a7todo)
		- [Java11新特性？todo](#java11%e6%96%b0%e7%89%b9%e6%80%a7todo)
	- [Java多线程](#java%e5%a4%9a%e7%ba%bf%e7%a8%8b)
		- [线程实现方法？](#%e7%ba%bf%e7%a8%8b%e5%ae%9e%e7%8e%b0%e6%96%b9%e6%b3%95)
		- [什么是线程安全的类？](#%e4%bb%80%e4%b9%88%e6%98%af%e7%ba%bf%e7%a8%8b%e5%ae%89%e5%85%a8%e7%9a%84%e7%b1%bb)
		- [如何保证线程安全？（线程同步方法）](#%e5%a6%82%e4%bd%95%e4%bf%9d%e8%af%81%e7%ba%bf%e7%a8%8b%e5%ae%89%e5%85%a8%e7%ba%bf%e7%a8%8b%e5%90%8c%e6%ad%a5%e6%96%b9%e6%b3%95)
		- [AtomicLong？](#atomiclong)
		- [LongAdder？](#longadder)
		- [AQS是什么？](#aqs%e6%98%af%e4%bb%80%e4%b9%88)
		- [ReentrantLock实现？](#reentrantlock%e5%ae%9e%e7%8e%b0)
		- [synchronized实现？](#synchronized%e5%ae%9e%e7%8e%b0)
		- [synchronized作用对象？](#synchronized%e4%bd%9c%e7%94%a8%e5%af%b9%e8%b1%a1)
		- [ReentrantLock和synchronized异同？](#reentrantlock%e5%92%8csynchronized%e5%bc%82%e5%90%8c)
		- [常见的锁优化方法？](#%e5%b8%b8%e8%a7%81%e7%9a%84%e9%94%81%e4%bc%98%e5%8c%96%e6%96%b9%e6%b3%95)
		- [轻量级锁和重量级锁是什么？锁膨胀过程是怎样的？](#%e8%bd%bb%e9%87%8f%e7%ba%a7%e9%94%81%e5%92%8c%e9%87%8d%e9%87%8f%e7%ba%a7%e9%94%81%e6%98%af%e4%bb%80%e4%b9%88%e9%94%81%e8%86%a8%e8%83%80%e8%bf%87%e7%a8%8b%e6%98%af%e6%80%8e%e6%a0%b7%e7%9a%84)
		- [偏向锁实现？](#%e5%81%8f%e5%90%91%e9%94%81%e5%ae%9e%e7%8e%b0)
		- [为什么wait、notify、notifyAll方法定义在Object类中？](#%e4%b8%ba%e4%bb%80%e4%b9%88waitnotifynotifyall%e6%96%b9%e6%b3%95%e5%ae%9a%e4%b9%89%e5%9c%a8object%e7%b1%bb%e4%b8%ad)
		- [为什么wait、notify、notifyAll方法必须在同步块中调用？](#%e4%b8%ba%e4%bb%80%e4%b9%88waitnotifynotifyall%e6%96%b9%e6%b3%95%e5%bf%85%e9%a1%bb%e5%9c%a8%e5%90%8c%e6%ad%a5%e5%9d%97%e4%b8%ad%e8%b0%83%e7%94%a8)
		- [线程池原理？常见的线程池？](#%e7%ba%bf%e7%a8%8b%e6%b1%a0%e5%8e%9f%e7%90%86%e5%b8%b8%e8%a7%81%e7%9a%84%e7%ba%bf%e7%a8%8b%e6%b1%a0)
		- [线程池的参数都有什么？](#%e7%ba%bf%e7%a8%8b%e6%b1%a0%e7%9a%84%e5%8f%82%e6%95%b0%e9%83%bd%e6%9c%89%e4%bb%80%e4%b9%88)
		- [线程池提交任务过程？](#%e7%ba%bf%e7%a8%8b%e6%b1%a0%e6%8f%90%e4%ba%a4%e4%bb%bb%e5%8a%a1%e8%bf%87%e7%a8%8b)
		- [线程池的reject策略？](#%e7%ba%bf%e7%a8%8b%e6%b1%a0%e7%9a%84reject%e7%ad%96%e7%95%a5)
		- [线程池运行状态？](#%e7%ba%bf%e7%a8%8b%e6%b1%a0%e8%bf%90%e8%a1%8c%e7%8a%b6%e6%80%81)
		- [HashMap多线程下为什么死锁？todo，动图](#hashmap%e5%a4%9a%e7%ba%bf%e7%a8%8b%e4%b8%8b%e4%b8%ba%e4%bb%80%e4%b9%88%e6%ad%bb%e9%94%81todo%e5%8a%a8%e5%9b%be)
		- [ConcurrentHashMap实现？1.7和1.8实现区别？](#concurrenthashmap%e5%ae%9e%e7%8e%b017%e5%92%8c18%e5%ae%9e%e7%8e%b0%e5%8c%ba%e5%88%ab)
		- [HashMap和ConcurrentHashMap有什么不同？](#hashmap%e5%92%8cconcurrenthashmap%e6%9c%89%e4%bb%80%e4%b9%88%e4%b8%8d%e5%90%8c)
		- [CopyOnWrite容器是什么？如何实现的？什么叫弱一致性？为什么读不需要加锁？适用场景？](#copyonwrite%e5%ae%b9%e5%99%a8%e6%98%af%e4%bb%80%e4%b9%88%e5%a6%82%e4%bd%95%e5%ae%9e%e7%8e%b0%e7%9a%84%e4%bb%80%e4%b9%88%e5%8f%ab%e5%bc%b1%e4%b8%80%e8%87%b4%e6%80%a7%e4%b8%ba%e4%bb%80%e4%b9%88%e8%af%bb%e4%b8%8d%e9%9c%80%e8%a6%81%e5%8a%a0%e9%94%81%e9%80%82%e7%94%a8%e5%9c%ba%e6%99%af)
		- [快速失败`fail-fast`和安全失败`fail-safe`？](#%e5%bf%ab%e9%80%9f%e5%a4%b1%e8%b4%a5fail-fast%e5%92%8c%e5%ae%89%e5%85%a8%e5%a4%b1%e8%b4%a5fail-safe)
		- [ThreadLocal是什么？怎么实现的？为什么会导致内存泄漏？](#threadlocal%e6%98%af%e4%bb%80%e4%b9%88%e6%80%8e%e4%b9%88%e5%ae%9e%e7%8e%b0%e7%9a%84%e4%b8%ba%e4%bb%80%e4%b9%88%e4%bc%9a%e5%af%bc%e8%87%b4%e5%86%85%e5%ad%98%e6%b3%84%e6%bc%8f)
		- [volatile 的两个特性？底层实现（具体到x86汇编）？](#volatile-%e7%9a%84%e4%b8%a4%e4%b8%aa%e7%89%b9%e6%80%a7%e5%ba%95%e5%b1%82%e5%ae%9e%e7%8e%b0%e5%85%b7%e4%bd%93%e5%88%b0x86%e6%b1%87%e7%bc%96)
		- [如何中断线程？interrupt做了什么？](#%e5%a6%82%e4%bd%95%e4%b8%ad%e6%96%ad%e7%ba%bf%e7%a8%8binterrupt%e5%81%9a%e4%ba%86%e4%bb%80%e4%b9%88)
	- [JVM](#jvm)
		- [JVM内存模型？](#jvm%e5%86%85%e5%ad%98%e6%a8%a1%e5%9e%8b)
		- [JVM内存分区？](#jvm%e5%86%85%e5%ad%98%e5%88%86%e5%8c%ba)
		- [GC如何确定要被回收的对象？](#gc%e5%a6%82%e4%bd%95%e7%a1%ae%e5%ae%9a%e8%a6%81%e8%a2%ab%e5%9b%9e%e6%94%b6%e7%9a%84%e5%af%b9%e8%b1%a1)
		- [有哪些`GC Roots`？](#%e6%9c%89%e5%93%aa%e4%ba%9bgc-roots)
		- [GC是如何回收空间（三种）？](#gc%e6%98%af%e5%a6%82%e4%bd%95%e5%9b%9e%e6%94%b6%e7%a9%ba%e9%97%b4%e4%b8%89%e7%a7%8d)
		- [何时会触发GC？](#%e4%bd%95%e6%97%b6%e4%bc%9a%e8%a7%a6%e5%8f%91gc)
		- [有哪些垃圾回收器？](#%e6%9c%89%e5%93%aa%e4%ba%9b%e5%9e%83%e5%9c%be%e5%9b%9e%e6%94%b6%e5%99%a8)
		- [介绍下CMS？](#%e4%bb%8b%e7%bb%8d%e4%b8%8bcms)
		- [介绍下G1？比CMS强在哪？](#%e4%bb%8b%e7%bb%8d%e4%b8%8bg1%e6%af%94cms%e5%bc%ba%e5%9c%a8%e5%93%aa)
		- [为什么区分新生代老年代？](#%e4%b8%ba%e4%bb%80%e4%b9%88%e5%8c%ba%e5%88%86%e6%96%b0%e7%94%9f%e4%bb%a3%e8%80%81%e5%b9%b4%e4%bb%a3)
		- [为什么Eden和Survivor的比例是8:1？为什么有两块Survivor？](#%e4%b8%ba%e4%bb%80%e4%b9%88eden%e5%92%8csurvivor%e7%9a%84%e6%af%94%e4%be%8b%e6%98%af81%e4%b8%ba%e4%bb%80%e4%b9%88%e6%9c%89%e4%b8%a4%e5%9d%97survivor)
		- [什么时候对象会进入老年代？](#%e4%bb%80%e4%b9%88%e6%97%b6%e5%80%99%e5%af%b9%e8%b1%a1%e4%bc%9a%e8%bf%9b%e5%85%a5%e8%80%81%e5%b9%b4%e4%bb%a3)
		- [类加载流程？](#%e7%b1%bb%e5%8a%a0%e8%bd%bd%e6%b5%81%e7%a8%8b)
		- [双亲委派模型？为什么需要双亲委派模型？怎么破坏双亲委派模型？为什么需要破坏？](#%e5%8f%8c%e4%ba%b2%e5%a7%94%e6%b4%be%e6%a8%a1%e5%9e%8b%e4%b8%ba%e4%bb%80%e4%b9%88%e9%9c%80%e8%a6%81%e5%8f%8c%e4%ba%b2%e5%a7%94%e6%b4%be%e6%a8%a1%e5%9e%8b%e6%80%8e%e4%b9%88%e7%a0%b4%e5%9d%8f%e5%8f%8c%e4%ba%b2%e5%a7%94%e6%b4%be%e6%a8%a1%e5%9e%8b%e4%b8%ba%e4%bb%80%e4%b9%88%e9%9c%80%e8%a6%81%e7%a0%b4%e5%9d%8f)
- [算法与数据结构](#%e7%ae%97%e6%b3%95%e4%b8%8e%e6%95%b0%e6%8d%ae%e7%bb%93%e6%9e%84)
	- [快排、堆排、归并、希尔、桶排序，实现？复杂度？最差情况？优势、适用场景？](#%e5%bf%ab%e6%8e%92%e5%a0%86%e6%8e%92%e5%bd%92%e5%b9%b6%e5%b8%8c%e5%b0%94%e6%a1%b6%e6%8e%92%e5%ba%8f%e5%ae%9e%e7%8e%b0%e5%a4%8d%e6%9d%82%e5%ba%a6%e6%9c%80%e5%b7%ae%e6%83%85%e5%86%b5%e4%bc%98%e5%8a%bf%e9%80%82%e7%94%a8%e5%9c%ba%e6%99%af)
	- [二叉树->二叉搜索树->平衡二叉搜索树->红黑树->B树->B+树，这个递进过程中的所有解决的问题和改进方法？](#%e4%ba%8c%e5%8f%89%e6%a0%91-%e4%ba%8c%e5%8f%89%e6%90%9c%e7%b4%a2%e6%a0%91-%e5%b9%b3%e8%a1%a1%e4%ba%8c%e5%8f%89%e6%90%9c%e7%b4%a2%e6%a0%91-%e7%ba%a2%e9%bb%91%e6%a0%91-b%e6%a0%91-b%e6%a0%91%e8%bf%99%e4%b8%aa%e9%80%92%e8%bf%9b%e8%bf%87%e7%a8%8b%e4%b8%ad%e7%9a%84%e6%89%80%e6%9c%89%e8%a7%a3%e5%86%b3%e7%9a%84%e9%97%ae%e9%a2%98%e5%92%8c%e6%94%b9%e8%bf%9b%e6%96%b9%e6%b3%95)
	- [90G文件，10G内存，怎么排序？](#90g%e6%96%87%e4%bb%b610g%e5%86%85%e5%ad%98%e6%80%8e%e4%b9%88%e6%8e%92%e5%ba%8f)
- [计算机网络](#%e8%ae%a1%e7%ae%97%e6%9c%ba%e7%bd%91%e7%bb%9c)
	- [四层模型、七层模型每一层是什么？做了什么？应用？](#%e5%9b%9b%e5%b1%82%e6%a8%a1%e5%9e%8b%e4%b8%83%e5%b1%82%e6%a8%a1%e5%9e%8b%e6%af%8f%e4%b8%80%e5%b1%82%e6%98%af%e4%bb%80%e4%b9%88%e5%81%9a%e4%ba%86%e4%bb%80%e4%b9%88%e5%ba%94%e7%94%a8)
	- [TCP/UDP区别？](#tcpudp%e5%8c%ba%e5%88%ab)
	- [TCP如何实现可靠传输？](#tcp%e5%a6%82%e4%bd%95%e5%ae%9e%e7%8e%b0%e5%8f%af%e9%9d%a0%e4%bc%a0%e8%be%93)
	- [TCP如何实现拥塞控制？](#tcp%e5%a6%82%e4%bd%95%e5%ae%9e%e7%8e%b0%e6%8b%a5%e5%a1%9e%e6%8e%a7%e5%88%b6)
	- [TCP三次握手过程？为什么需要三次握手？](#tcp%e4%b8%89%e6%ac%a1%e6%8f%a1%e6%89%8b%e8%bf%87%e7%a8%8b%e4%b8%ba%e4%bb%80%e4%b9%88%e9%9c%80%e8%a6%81%e4%b8%89%e6%ac%a1%e6%8f%a1%e6%89%8b)
	- [TCP四次挥手过程？为什么需要四次挥手？最后的等待确认时间为什么是2*MSL(Maximum Segment Lifetime)？](#tcp%e5%9b%9b%e6%ac%a1%e6%8c%a5%e6%89%8b%e8%bf%87%e7%a8%8b%e4%b8%ba%e4%bb%80%e4%b9%88%e9%9c%80%e8%a6%81%e5%9b%9b%e6%ac%a1%e6%8c%a5%e6%89%8b%e6%9c%80%e5%90%8e%e7%9a%84%e7%ad%89%e5%be%85%e7%a1%ae%e8%ae%a4%e6%97%b6%e9%97%b4%e4%b8%ba%e4%bb%80%e4%b9%88%e6%98%af2mslmaximum-segment-lifetime)
	- [TIME_WAIT是什么？为什么要有TIME_WAIT？TIME_WAIT状态的连接过多会怎么样？怎么解决TIME_WAIT过多的问题？](#timewait%e6%98%af%e4%bb%80%e4%b9%88%e4%b8%ba%e4%bb%80%e4%b9%88%e8%a6%81%e6%9c%89timewaittimewait%e7%8a%b6%e6%80%81%e7%9a%84%e8%bf%9e%e6%8e%a5%e8%bf%87%e5%a4%9a%e4%bc%9a%e6%80%8e%e4%b9%88%e6%a0%b7%e6%80%8e%e4%b9%88%e8%a7%a3%e5%86%b3timewait%e8%bf%87%e5%a4%9a%e7%9a%84%e9%97%ae%e9%a2%98)
	- [CLOSE_WAIT是什么？为什么可能出现很多CLOSE_WAIT状态？CLOSE_WAIT状态的连接过多会怎么样？怎么解决CLOSE_WAIT过多的问题？](#closewait%e6%98%af%e4%bb%80%e4%b9%88%e4%b8%ba%e4%bb%80%e4%b9%88%e5%8f%af%e8%83%bd%e5%87%ba%e7%8e%b0%e5%be%88%e5%a4%9aclosewait%e7%8a%b6%e6%80%81closewait%e7%8a%b6%e6%80%81%e7%9a%84%e8%bf%9e%e6%8e%a5%e8%bf%87%e5%a4%9a%e4%bc%9a%e6%80%8e%e4%b9%88%e6%a0%b7%e6%80%8e%e4%b9%88%e8%a7%a3%e5%86%b3closewait%e8%bf%87%e5%a4%9a%e7%9a%84%e9%97%ae%e9%a2%98)
	- [GET/POST异同？](#getpost%e5%bc%82%e5%90%8c)
	- [浏览器输入域名到返回页面发生了什么？](#%e6%b5%8f%e8%a7%88%e5%99%a8%e8%be%93%e5%85%a5%e5%9f%9f%e5%90%8d%e5%88%b0%e8%bf%94%e5%9b%9e%e9%a1%b5%e9%9d%a2%e5%8f%91%e7%94%9f%e4%ba%86%e4%bb%80%e4%b9%88)
	- [网络层协议ARP的工作流程？](#%e7%bd%91%e7%bb%9c%e5%b1%82%e5%8d%8f%e8%ae%aearp%e7%9a%84%e5%b7%a5%e4%bd%9c%e6%b5%81%e7%a8%8b)
	- [Cookie和Session的异同](#cookie%e5%92%8csession%e7%9a%84%e5%bc%82%e5%90%8c)
	- [跨站脚本攻击(Xss, Cross Site Scripting)](#%e8%b7%a8%e7%ab%99%e8%84%9a%e6%9c%ac%e6%94%bb%e5%87%bbxss-cross-site-scripting)
	- [HTTP常见状态码？200/201/202/204/206/301/302/304/400/401/403/404/500？](#http%e5%b8%b8%e8%a7%81%e7%8a%b6%e6%80%81%e7%a0%81200201202204206301302304400401403404500)
	- [HTTP1.1/2.0各自改进了什么？](#http1120%e5%90%84%e8%87%aa%e6%94%b9%e8%bf%9b%e4%ba%86%e4%bb%80%e4%b9%88)
	- [HTTP长连接短连接？](#http%e9%95%bf%e8%bf%9e%e6%8e%a5%e7%9f%ad%e8%bf%9e%e6%8e%a5)
	- [对称加密和与非对称加密？](#%e5%af%b9%e7%a7%b0%e5%8a%a0%e5%af%86%e5%92%8c%e4%b8%8e%e9%9d%9e%e5%af%b9%e7%a7%b0%e5%8a%a0%e5%af%86)
	- [介绍HTTPS？](#%e4%bb%8b%e7%bb%8dhttps)
	- [为什么HTTPS使用混合加密？](#%e4%b8%ba%e4%bb%80%e4%b9%88https%e4%bd%bf%e7%94%a8%e6%b7%b7%e5%90%88%e5%8a%a0%e5%af%86)
	- [HTTPS握手过程？如何保证安全性？](#https%e6%8f%a1%e6%89%8b%e8%bf%87%e7%a8%8b%e5%a6%82%e4%bd%95%e4%bf%9d%e8%af%81%e5%ae%89%e5%85%a8%e6%80%a7)
- [操作系统](#%e6%93%8d%e4%bd%9c%e7%b3%bb%e7%bb%9f)
	- [进程、线程](#%e8%bf%9b%e7%a8%8b%e7%ba%bf%e7%a8%8b)
		- [进程和线程是什么？](#%e8%bf%9b%e7%a8%8b%e5%92%8c%e7%ba%bf%e7%a8%8b%e6%98%af%e4%bb%80%e4%b9%88)
		- [进程、线程区别？](#%e8%bf%9b%e7%a8%8b%e7%ba%bf%e7%a8%8b%e5%8c%ba%e5%88%ab)
		- [进程间通信方式（IPC，Inter-Process Communication）](#%e8%bf%9b%e7%a8%8b%e9%97%b4%e9%80%9a%e4%bf%a1%e6%96%b9%e5%bc%8fipcinter-process-communication)
		- [进程调度策略？](#%e8%bf%9b%e7%a8%8b%e8%b0%83%e5%ba%a6%e7%ad%96%e7%95%a5)
		- [进程间同步方式？](#%e8%bf%9b%e7%a8%8b%e9%97%b4%e5%90%8c%e6%ad%a5%e6%96%b9%e5%bc%8f)
	- [死锁](#%e6%ad%bb%e9%94%81)
		- [什么是死锁？](#%e4%bb%80%e4%b9%88%e6%98%af%e6%ad%bb%e9%94%81)
		- [死锁产生的条件？](#%e6%ad%bb%e9%94%81%e4%ba%a7%e7%94%9f%e7%9a%84%e6%9d%a1%e4%bb%b6)
		- [怎么处理死锁？](#%e6%80%8e%e4%b9%88%e5%a4%84%e7%90%86%e6%ad%bb%e9%94%81)
		- [银行家算法是什么？](#%e9%93%b6%e8%a1%8c%e5%ae%b6%e7%ae%97%e6%b3%95%e6%98%af%e4%bb%80%e4%b9%88)
		- [资源分配图是什么？](#%e8%b5%84%e6%ba%90%e5%88%86%e9%85%8d%e5%9b%be%e6%98%af%e4%bb%80%e4%b9%88)
	- [虚存、页式存储](#%e8%99%9a%e5%ad%98%e9%a1%b5%e5%bc%8f%e5%ad%98%e5%82%a8)
		- [虚拟内存的寻址过程？](#%e8%99%9a%e6%8b%9f%e5%86%85%e5%ad%98%e7%9a%84%e5%af%bb%e5%9d%80%e8%bf%87%e7%a8%8b)
		- [什么是缺页中断？页面置换算法？抖动是什么？](#%e4%bb%80%e4%b9%88%e6%98%af%e7%bc%ba%e9%a1%b5%e4%b8%ad%e6%96%ad%e9%a1%b5%e9%9d%a2%e7%bd%ae%e6%8d%a2%e7%ae%97%e6%b3%95%e6%8a%96%e5%8a%a8%e6%98%af%e4%bb%80%e4%b9%88)
	- [Linux](#linux)
		- [常用指令，参考Linux章节](#%e5%b8%b8%e7%94%a8%e6%8c%87%e4%bb%a4%e5%8f%82%e8%80%83linux%e7%ab%a0%e8%8a%82)
- [设计模式](#%e8%ae%be%e8%ae%a1%e6%a8%a1%e5%bc%8f)
	- [设计模式的七大设计原则？](#%e8%ae%be%e8%ae%a1%e6%a8%a1%e5%bc%8f%e7%9a%84%e4%b8%83%e5%a4%a7%e8%ae%be%e8%ae%a1%e5%8e%9f%e5%88%99)
	- [单例模式？Java的几种写法？](#%e5%8d%95%e4%be%8b%e6%a8%a1%e5%bc%8fjava%e7%9a%84%e5%87%a0%e7%a7%8d%e5%86%99%e6%b3%95)
	- [为什么上面的双重检查的方法要使用volatile关键字？](#%e4%b8%ba%e4%bb%80%e4%b9%88%e4%b8%8a%e9%9d%a2%e7%9a%84%e5%8f%8c%e9%87%8d%e6%a3%80%e6%9f%a5%e7%9a%84%e6%96%b9%e6%b3%95%e8%a6%81%e4%bd%bf%e7%94%a8volatile%e5%85%b3%e9%94%ae%e5%ad%97)
	- [为什么使用内部类静态变量可以实现单例模式？](#%e4%b8%ba%e4%bb%80%e4%b9%88%e4%bd%bf%e7%94%a8%e5%86%85%e9%83%a8%e7%b1%bb%e9%9d%99%e6%80%81%e5%8f%98%e9%87%8f%e5%8f%af%e4%bb%a5%e5%ae%9e%e7%8e%b0%e5%8d%95%e4%be%8b%e6%a8%a1%e5%bc%8f)
	- [为什么使用枚举类型可以实现单例模式](#%e4%b8%ba%e4%bb%80%e4%b9%88%e4%bd%bf%e7%94%a8%e6%9e%9a%e4%b8%be%e7%b1%bb%e5%9e%8b%e5%8f%af%e4%bb%a5%e5%ae%9e%e7%8e%b0%e5%8d%95%e4%be%8b%e6%a8%a1%e5%bc%8f)
	- [如何破坏单例模式？](#%e5%a6%82%e4%bd%95%e7%a0%b4%e5%9d%8f%e5%8d%95%e4%be%8b%e6%a8%a1%e5%bc%8f)
	- [观察者模式](#%e8%a7%82%e5%af%9f%e8%80%85%e6%a8%a1%e5%bc%8f)
	- [装饰器模式](#%e8%a3%85%e9%a5%b0%e5%99%a8%e6%a8%a1%e5%bc%8f)
- [Spring相关](#spring%e7%9b%b8%e5%85%b3)
- [MySQL](#mysql)
	- [基本的SQL语句，CRUD？设置索引？](#%e5%9f%ba%e6%9c%ac%e7%9a%84sql%e8%af%ad%e5%8f%a5crud%e8%ae%be%e7%bd%ae%e7%b4%a2%e5%bc%95)
	- [三范式？](#%e4%b8%89%e8%8c%83%e5%bc%8f)
	- [MyISAM和InnoDB特点、区别、使用场景？](#myisam%e5%92%8cinnodb%e7%89%b9%e7%82%b9%e5%8c%ba%e5%88%ab%e4%bd%bf%e7%94%a8%e5%9c%ba%e6%99%af)
	- [创建索引的SQL语句？实现方法？](#%e5%88%9b%e5%bb%ba%e7%b4%a2%e5%bc%95%e7%9a%84sql%e8%af%ad%e5%8f%a5%e5%ae%9e%e7%8e%b0%e6%96%b9%e6%b3%95)
	- [什么是聚簇索引？](#%e4%bb%80%e4%b9%88%e6%98%af%e8%81%9a%e7%b0%87%e7%b4%a2%e5%bc%95)
	- [什么是覆盖索引？](#%e4%bb%80%e4%b9%88%e6%98%af%e8%a6%86%e7%9b%96%e7%b4%a2%e5%bc%95)
	- [主键和唯一索引的区别？](#%e4%b8%bb%e9%94%ae%e5%92%8c%e5%94%af%e4%b8%80%e7%b4%a2%e5%bc%95%e7%9a%84%e5%8c%ba%e5%88%ab)
	- [哪些键适合建索引？](#%e5%93%aa%e4%ba%9b%e9%94%ae%e9%80%82%e5%90%88%e5%bb%ba%e7%b4%a2%e5%bc%95)
	- [什么时候索引会失效？](#%e4%bb%80%e4%b9%88%e6%97%b6%e5%80%99%e7%b4%a2%e5%bc%95%e4%bc%9a%e5%a4%b1%e6%95%88)
	- [B树是什么？](#b%e6%a0%91%e6%98%af%e4%bb%80%e4%b9%88)
	- [B+树改进？这个索引结构为什么有效？](#b%e6%a0%91%e6%94%b9%e8%bf%9b%e8%bf%99%e4%b8%aa%e7%b4%a2%e5%bc%95%e7%bb%93%e6%9e%84%e4%b8%ba%e4%bb%80%e4%b9%88%e6%9c%89%e6%95%88)
	- [什么是Hash索引？为什么InnoDB没有显式的Hash索引？](#%e4%bb%80%e4%b9%88%e6%98%afhash%e7%b4%a2%e5%bc%95%e4%b8%ba%e4%bb%80%e4%b9%88innodb%e6%b2%a1%e6%9c%89%e6%98%be%e5%bc%8f%e7%9a%84hash%e7%b4%a2%e5%bc%95)
	- [事务的四个基本特性？](#%e4%ba%8b%e5%8a%a1%e7%9a%84%e5%9b%9b%e4%b8%aa%e5%9f%ba%e6%9c%ac%e7%89%b9%e6%80%a7)
	- [事务的隔离级别？](#%e4%ba%8b%e5%8a%a1%e7%9a%84%e9%9a%94%e7%a6%bb%e7%ba%a7%e5%88%ab)
	- [MVCC是什么？解决了什么问题？](#mvcc%e6%98%af%e4%bb%80%e4%b9%88%e8%a7%a3%e5%86%b3%e4%ba%86%e4%bb%80%e4%b9%88%e9%97%ae%e9%a2%98)
	- [InnoDB插入数据时发生了什么？](#innodb%e6%8f%92%e5%85%a5%e6%95%b0%e6%8d%ae%e6%97%b6%e5%8f%91%e7%94%9f%e4%ba%86%e4%bb%80%e4%b9%88)
	- [InnoDB如何保证事务持久性的？](#innodb%e5%a6%82%e4%bd%95%e4%bf%9d%e8%af%81%e4%ba%8b%e5%8a%a1%e6%8c%81%e4%b9%85%e6%80%a7%e7%9a%84)
	- [MySQL复制过程？](#mysql%e5%a4%8d%e5%88%b6%e8%bf%87%e7%a8%8b)
	- [drop、delete与truncate的区别](#dropdelete%e4%b8%8etruncate%e7%9a%84%e5%8c%ba%e5%88%ab)
	- [什么是视图？](#%e4%bb%80%e4%b9%88%e6%98%af%e8%a7%86%e5%9b%be)
	- [什么是游标？](#%e4%bb%80%e4%b9%88%e6%98%af%e6%b8%b8%e6%a0%87)
	- [什么是触发器？](#%e4%bb%80%e4%b9%88%e6%98%af%e8%a7%a6%e5%8f%91%e5%99%a8)
- [Redis](#redis)
- [分布式系统](#%e5%88%86%e5%b8%83%e5%bc%8f%e7%b3%bb%e7%bb%9f)

# Java

## 基础

### Java的四大特性？

抽象，封装，继承，多态；

- 封装：把数据和过程封装成一个整体（类），尽可能隐藏细节，只对外提供一系列接口

- 继承：一种代码重用的机制；子类可以定制自己特殊的功能，对于其他的功能可以复用父类已有的实现

- 多态：指相同的函数在不同类型的对象上并获得不同的结果，这种现象称为多态

- 抽象：将对象共有的特性抽象为一个独立的部分，限定了一系列接口而不提供具体的实现。在Java中使用abstract关键字修饰，或者使用interface达到抽象的目的

### `==`和hashCode和equals？为什么要重写equals方法？

`==`用于判断两个对象的地址，hashCode用于计算对象的哈希码，而equals方法用于判断两个对象是否是语义上的相等（例如两个对象有不同的地址但是包含了一模一样的成员变量，一般情况下我们都认为这两个对象是equal的）。

因为equals默认由Object对象实现为内存地址，如果我们希望让自己的类实现期望的判断相等的功能，那么需要重写这个方法。

### HashMap中使用可变对象作为key需要注意什么？

要保证可变对象的hashCode是不变的。因为HashMap依赖于hashCode查找，如果hashCode发生了变化，则该键值对就丢失了。

### Java的集合类整体接口和抽象类设计？

最基础接口为Collection，Set/List/Queue接口扩展了Collection接口。

抽象类AbstractCollection实现了Collection接口，AbstractSet/AbstractList/AbstractQueue都继承了AbstractCollection并实现了对应接口。

再往下层就是具体的集合类实现。

![](.java面试相关.asset/2019-03-02-11-43-13-java面试相关.png)

### ArrayList、LinkedList基础空间、扩展方案？

ArrayList：内部是`Object[]`，初始容量10，每次扩容为会将容量增大至原来的1.5倍，扩容是通过新建一个数组然后拷贝原数据实现的；可以通过trimToSize将容量缩减至数据量大小；也可以通过ensureCapacity扩容至制定容量

LinkedList：双向链表

### HashMap实现？添加过程？rehash过程？Treeify？加载因子？初始容量？resize规则？Hash策略？为什么容量为2的n次幂？put策略？

实现的主体是Node数组：`Node<K,V>[] table;`

JDK1.8及以后考虑到查询效率和存储空间（树节点比链表节点大）两方面原因，链表在到达阈值时会和红黑树相互转化（阈值分别为8和6），这个转化阈值是根据HashCode的泊松分布概率设置的，一个桶中节点达到八个的概率只有小数点后千万分之一。

加载因子默认0.75，初始容量16，每次扩容将桶容量翻倍。

Resize: 1.7以前版本扩容需要重新计算Hash放到新桶里，并且由于是头插法（put和transfer方法都是头插），会导致死锁；1.8及以后版本改成了尾插法（put采用尾插），不需要重新计算Hash，放弃了transfer方法（改为了将一个链表按Hash拆成需要转移到新桶和不需要转移的两部分，不改变Node顺序），避免了之前死循环死锁的情况。下面是1.8中Hashmap的resize过程对于链表的处理：

```java
do {
	next = e.next;
	// 先验条件：oldCap是2的n次幂；
	// 这个判断条件的意思是：如果当前element的hash和oldCap中1的位数不同为1，则说明其`e.hash & (oldCap << 1)`和`e.hash & oldCap`是相等的，也就是说不需要将该节点移动至扩容后的新的桶中。
	// 将一个旧链表通过如上判断拆成了两个新链表，随后存储到新table中对应的位置。
	if ((e.hash & oldCap) == 0) {
		if (loTail == null)
			loHead = e;
		else
			loTail.next = e;
		loTail = e;
	}
	else {
		if (hiTail == null)
			hiHead = e;
		else
			hiTail.next = e;
		hiTail = e;
	}
} while ((e = next) != null);
if (loTail != null) {
	loTail.next = null;
	newTab[j] = loHead;
}
if (hiTail != null) {
	hiTail.next = null;
	newTab[j + oldCap] = hiHead;
}
```

Hash策略：`(key == null) ? 0 : (h = key.hashCode()) ^ (h >>> 16)`,这是为了把高位bit传播到低位bit，避免只有低位bit变化时且Bucket较少时的碰撞。

容量设置为2的n次幂有如下几个考虑：

- 取余操作可以通过`hashCode & (capacity-1)`这个位运算来加速，大部分操作也都可以使用位操作加速
- 为了尽可能地减少Hash碰撞
- resize操作依赖于容量翻倍这个先决条件，

### 1.7和1.8的HashMap区别？

- 插入方法：1.7头插，1.8尾插
- resize：1.7会rehash；1.8不会rehash，只判断是否会是否将已有节点放入新桶
- 链表与红黑树相互转化

### HashMap与HashTable区别？

- HashMap是AbstractMap的子类，HashTable是Dictionary子类

- HashMap支持null key和null value

- HashMap非线程安全，Hashtable线程安全（所有方法synchronized修饰）

- HashMap先插入再扩容，HashTable先扩容再插入

### Java中的异常机制？

整体结构图在最后，这里简述一下各个异常的区别：

- Throwable

	- Error：一般是虚拟机自身问题，例如OOM/SOF一类的JVM错误，程序员无能为力
	- Exception
		- RuntimeException
		
			运行时异常，最典型的就是NPE，这类异常一般来说可以通过严谨的变成避免，所以不需要手动检查(check)。
		
		- IOException/ClassNotFoundException/CloneNotSupportedException
		
			这些异常又总称`Checked Exception`，即出现有可能出现这些异常时一定要显式编程处理(抛出或try/catch处理)，因为这些异常很可能出现，影响程序运行，并且无法通过编程的手段避免。

![](.java面试相关.asset/2019-03-16-15-13-22-java面试相关.png)

### BIO/NIO/AIO？todo

### Java8的函数式接口？

简单地说，就是只有一个抽象方法的接口，用于支持lamda表达式，进一步取代大部分场景下的匿名类。JDK中的Runnable/Callable/Comparator都是典型的函数式接口。

Java8提供`@FunctionalInterface`标识函数式接口，但这个注解是非必须的，只要是只含有一个方法的接口都会被JVM判断为函数式接口。这个注解主要用于避免开发人员向接口中添加新的方法。

### Java9的新特性？todo

### Java10新特性？todo

### Java11新特性？todo

## Java多线程

### 线程实现方法？

实现Runnable/Callable接口、继承Thread、线程池

### 什么是线程安全的类？

核心在于正确性。当多个线程访问该类时，无论系统如何调度，线程如何交替，在主代码不进行任何同步操作的情况下，该类总能保证正确的行为


### 如何保证线程安全？（线程同步方法）

- 加锁

- 无锁方法 volatile/CAS

- 使用不变类或线程安全类（Atomic、并发容器）

### AtomicLong？

通过维护一个volatile long值和一组CAS方法（getAndAdd/incrementAndGet）保证原子性的类。

### LongAdder？

类似AtomicLong，但是通过维护一组Cell来应对高并发的情况，一般用并发求和的场景。

### AQS是什么？

抽象队列同步器`AbstractQueuedSynchronizer`主体是一个代表资源的`volatile int state`成员变量和一个存储等待线程的双向链表，以及一组访问对象的方法。

![](.java面试相关.asset/2019-03-29-15-13-34-java面试相关.png)

Doug Lea老爷子设定了三个访问state的方法：

- `getState()`
- `setState()`
- `boolean compareAndSetState(int expect, int update)`

AQS是一个抽象类，其实现了线程等待队列的维护（获取资源失败入队、唤醒出队）等基本操作。

队列的每一个节点存储了线程获取资源的方式（`Exclusive`和`Share`两种）和当前节点的等待状态（不同于线程状态），状态有三种，如下（来源于`AbstractQueuedSynchronizer$Node`注释）。

```java
/**
 * Status field, taking on only the values:
 *   SIGNAL:     The successor of this node is (or will soon be)
 *               blocked (via park), so the current node must
 *               unpark its successor when it releases or
 *               cancels. To avoid races, acquire methods must
 *               first indicate they need a signal,
 *               then retry the atomic acquire, and then,
 *               on failure, block.
 *   CANCELLED:  This node is cancelled due to timeout or interrupt.
 *               Nodes never leave this state. In particular,
 *               a thread with cancelled node never again blocks.
 *   CONDITION:  This node is currently on a condition queue.
 *               It will not be used as a sync queue node
 *               until transferred, at which time the status
 *               will be set to 0. (Use of this value here has
 *               nothing to do with the other uses of the
 *               field, but simplifies mechanics.)
 *   PROPAGATE:  A releaseShared should be propagated to other
 *               nodes. This is set (for head node only) in
 *               doReleaseShared to ensure propagation
 *               continues, even if other operations have
 *               since intervened.
 *   0:          None of the above
 *
 * The values are arranged numerically to simplify use.
 * Non-negative values mean that a node doesn't need to
 * signal. So, most code doesn't need to check for particular
 * values, just for sign.
 *
 * The field is initialized to 0 for normal sync nodes, and
 * CONDITION for condition nodes.  It is modified using CAS
 * (or when possible, unconditional volatile writes).
 */
volatile int waitStatus;
```

其实现类主要需要实现state的获取与释放方式（公平/非公平、读写锁等），需要重写的方法如下：

- `isHeldExclusively()`：判断当前线程是否正独占资源
- `tryAcquire(int)`：独占获取
- `tryRelease(int)`：独占释放
- `tryAcquireShared(int)`：共享获取
- `tryReleaseShared(int)`：共享释放

AQS已经实现了一些顶层基础方法：

- `acquire(int)`

	负责获取资源、入队操作。

	```java
	public final void acquire(int arg) {
		if (!tryAcquire(arg) &&
			acquireQueued(addWaiter(Node.EXCLUSIVE), arg))
			// selfInterrupt就是Thread.currentThread().interrupt();
			selfInterrupt();
	}

	private Node addWaiter(Node mode) {
		// 以给定模式构造结点。mode有两种：EXCLUSIVE（独占）和SHARED（共享）
		Node node = new Node(Thread.currentThread(), mode);
		
		// 尝试快速方式直接放到队尾。
		Node pred = tail;
		if (pred != null) {
			node.prev = pred;
			if (compareAndSetTail(pred, node)) {
				pred.next = node;
				return node;
			}
		}
		
		// 上一步失败则通过enq入队，enq就是一个自旋操作
		enq(node);
		return node;
	}

	// 在队列中时自旋检查当前线程是被唤醒的，并尝试获取资源，这个函数名其实叫做acquireInQueue更直观一些
	final boolean acquireQueued(final Node node, int arg) {
		boolean failed = true;// 标记是否成功拿到资源
		try {
			boolean interrupted = false;// 标记等待过程中是否被中断过
			
			// 自旋
			for (;;) {
				final Node p = node.predecessor();	// 拿到前驱
				// 如果前驱是head，即该结点已成老二，那么便有资格去尝试获取资源（可能是老大释放完资源唤醒自己的，当然也可能被interrupt了）。
				if (p == head && tryAcquire(arg)) {
					setHead(node);	// 拿到资源后，将head指向该结点。所以head所指的标杆结点，就是当前获取到资源的那个结点或null。
					p.next = null; 	// setHead中node.prev已置为null，此处再将head.next置为null，就是为了方便GC回收以前的head结点。也就意味着之前拿完资源的结点出队了！
					failed = false;
					return interrupted;	// 返回等待过程中是否被中断过
				}
				
				// 如果自己可以休息了，就进入waiting状态，直到被unpark()
				if (shouldParkAfterFailedAcquire(p, node) &&
					parkAndCheckInterrupt())
					interrupted = true;	// 如果等待过程中被中断过，哪怕只有那么一次，就将interrupted标记为true
			}
		} finally {
			if (failed)
				cancelAcquire(node);
		}
	}
	```

	![](.java面试相关.asset/2019-03-29-17-05-32-java面试相关.png)

- `release(int)`

	负责释放资源、唤醒队列中的后继线程。

	```java
	public final boolean release(int arg) {
        if (tryRelease(arg)) {
            Node h = head;
            if (h != null && h.waitStatus != 0)
                unparkSuccessor(h);
            return true;
        }
        return false;
    }

	private void unparkSuccessor(Node node) {
    	// 这里，node一般为当前线程所在的结点。
		int ws = node.waitStatus;
		if (ws < 0)// 置零当前线程所在的结点状态，允许失败。
			compareAndSetWaitStatus(node, ws, 0);

		Node s = node.next;// 找到下一个需要唤醒的结点s
		if (s == null || s.waitStatus > 0) {// 如果为空或已取消
			s = null;
			for (Node t = tail; t != null && t != node; t = t.prev)
				if (t.waitStatus <= 0)// 从这里可以看出，<=0的结点，都是还有效的结点。
					s = t;
		}
		if (s != null)
			LockSupport.unpark(s.thread);// 唤醒
	}
	```

参考[Java并发之AQS详解](https://www.cnblogs.com/waterystone/p/4920797.html)

### ReentrantLock实现？

委托给AQS实现，通过重写了AQS的锁获取和释放策略提供了不同形式的锁。

### synchronized实现？

对应到字节码上是`monitorenter`/`monitorexit`两条指令，分别代表着使当前线程获取到对象的锁和释放对象的锁，其机制基于监视器，具体实现是通过控制对象头的`Mark Word`实现的。

### synchronized作用对象？

类静态方法（类锁）、类（类锁）、实例方法（对象锁）、实例（对象锁）

### ReentrantLock和synchronized异同？

最大的区别是一个是锁，一个是Java的关键字。

实现方式上ReentrantLock使用的是`LockSupport`提供的`park`/`unpark`方法实现阻塞和释放阻塞进程；synchronized使用的是监视器`Monitor`实现的阻塞与释放

synchronized可以自动获取释放，ReentrantLock需要手动释放，如果忘记释放或异常时未释放可能会死锁。

除此以外，ReentrantLock可以有很多参数：中断取锁、多条件、公平锁

### 常见的锁优化方法？

- 自旋锁
- 锁粗化：一个大锁优于多个小锁
- 偏向锁：消除在无竞争环境下的同步原语，判断依据来自于逃逸分析
- 锁消除：JVM编译优化，将不存在竞争的锁消除

### 轻量级锁和重量级锁是什么？锁膨胀过程是怎样的？

简而言之，重量级锁是使用操作系统互斥量实现的锁；轻量级锁是指在低冲突情况下，通过CAS方法避免互斥锁一类的重量级锁而造成的大量开销，其加锁、检查过程如下：

每一个对象头会有一个`Mark Word`的区域，其中有2bit记录了锁定状态，默认为未锁定`01`。`Mark Word`是一个非固定数据区域，在不同情况下储存了不同信息。

1. 代码进入同步块时，在当前线程的栈帧中创建一个对象的`Mark Word`的副本，称为`Lock Record`

2. 然后通过CAS将`Mark Word`修改为栈帧上的`Lock Record`的引用，如果成功进行步骤3，如果失败进行步骤4

3. CAS写入成功，表明当前线程拥有了该对象的锁，此时将对象的锁定状态修改为轻量级锁`00`

4. CAS写入失败，JVM先检查当前线程是否已经获得了轻量级锁，如果有则继续执行；如果没有，则表明发生了冲突，此时进行锁膨胀将轻量级锁升级为重量级锁，即将锁定状态置为重量级锁`10`

### 偏向锁实现？

在轻量级锁的基础上还可以进一步降低开销，它将第一个获取对象锁的线程的ID通过CAS方法写入`Mark Word`，随后JVM每次在该线程下使用该类可以不再使用CAS检查。一旦有其他线程介入，则偏向模式结束，进入轻量级锁状态。

### 为什么wait、notify、notifyAll方法定义在Object类中？

因为所有对象都要持有锁，每一个类对象实质上是一个`Monitor`，这些方法都是`Monitor`的实现。

### 为什么wait、notify、notifyAll方法必须在同步块中调用？

因为这三个方法依赖于进入同步块时构建的`Monitor`来维持正确的行为。

### 线程池原理？常见的线程池？

线程池维护了一组工作线程`HashSet<Worker> workers`、一个用于存放任务的阻塞队列`BlockingQueue<Runnable> workQueue`。线程池可以避免频繁的线程创建与销毁，降低系统资源的消耗；固定的线程数量也方便管理。

JDK推荐使用`Executors`类的静态工厂方法创建常见的线程池，这些线程池实际上都是`ThreadPoolExecutor`的参数不同的实例，常见的线程池如下：

- FixedThreadPool：创建固定线程数的线程池

- SingleThreadExecutor：创建只有一个工作线程的线程池

- CachedThreadPool：线程上下限分别为0和Integer.MAX_VALUE，也就是说线程是无限的；没有阻塞队列，每一个新任务必须等到有空闲线程，否则阻塞。

- ForkAndJoinPool

- 线程池调优：
	
	- 设置最大线程数，防止系统资源耗尽
	
	- 针对不同类别任务设置不同线程数：CPU型任务（线程数量=内核数量），IO型任务（线程数量=内核数量*2）

### 线程池的参数都有什么？

直接贴线程池类`ThreadPoolExecutor`的注释了。

```java
/**
* Creates a new {@code ThreadPoolExecutor} with the given initial
* parameters.
*
* @param corePoolSize the number of threads to keep in the pool, even
*        if they are idle, unless {@code allowCoreThreadTimeOut} is set
* @param maximumPoolSize the maximum number of threads to allow in the
*        pool
* @param keepAliveTime when the number of threads is greater than
*        the core, this is the maximum time that excess idle threads
*        will wait for new tasks before terminating.
* @param unit the time unit for the {@code keepAliveTime} argument
* @param workQueue the queue to use for holding tasks before they are
*        executed.  This queue will hold only the {@code Runnable}
*        tasks submitted by the {@code execute} method.
* @param threadFactory the factory to use when the executor
*        creates a new thread 用于设定线程优先级、名字、分组、是否为守护线程等
* @param handler the handler to use when execution is blocked
*        because the thread bounds and queue capacities are reached
* @throws IllegalArgumentException if one of the following holds:<br>
*         {@code corePoolSize < 0}<br>
*         {@code keepAliveTime < 0}<br>
*         {@code maximumPoolSize <= 0}<br>
*         {@code maximumPoolSize < corePoolSize}
* @throws NullPointerException if {@code workQueue}
*         or {@code threadFactory} or {@code handler} is null
*/

public ThreadPoolExecutor(int corePoolSize,
                        	int maximumPoolSize,
                        	long keepAliveTime,
                        	TimeUnit unit,
                        	BlockingQueue<Runnable> workQueue,
                        	ThreadFactory threadFactory,
                        	RejectedExecutionHandler handler);
```

### 线程池提交任务过程？

援引JDK1.8`ThreadPoolExecutor`注释：

> When a new task is submitted in method {@link #execute(Runnable)}, and fewer than corePoolSize threads are running, a new thread is created to handle the request, even if other worker threads are idle.  If there are more than corePoolSize but less than maximumPoolSize threads running, a new thread will be created only if the queue is full.  By setting corePoolSize and maximumPoolSize the same, you create a fixed-size thread pool. By setting maximumPoolSize to an essentially unbounded value such as {@code Integer.MAX_VALUE}, you allow the pool to accommodate an arbitrary number of concurrent tasks. Most typically, core and maximum pool sizes are set only upon construction, but they may also be changed dynamically using {@link #setCorePoolSize} and {@link #setMaximumPoolSize}.

### 线程池的reject策略？

四种：

- `AbortPolicy`：抛出`RejectedExecutionException`异常
- `CallerRunsPolicy`：使用调用者线程执行
- `DiscardPolicy`：丢弃这个提交的任务
- `DiscardOldestPolicy`：丢弃队列中最老的任务，然后重新提交任务（可能重复触发该策略）

### 线程池运行状态？

- `RUNNING`：接收新任务并处理队列积压的任务
- `SHUTDOWN`：不再接收新任务，但是会继续处理队列中的任务。调用`shutdown()`进入该状态
- `STOP`：中断正在执行的任务，不再接收新任务，不再处理队列中的任务。调用`shutdownNow()`进入该状态
- `TIDYING`：所有的任务都结束后进入`TIDYING`状态，执行`terminate()`方法
- `TERMINATED`：`terminated()`执行结束

### HashMap多线程下为什么死锁？todo，动图

JDK1.7使用头插法，JDK1.8采用尾插法。采用头插法并且有并发时如果发生了扩容，可能会导致桶上的链表出现环。采用尾插法后不会出现这个问题了，因为只有一次遍历过程。

### ConcurrentHashMap实现？1.7和1.8实现区别？

- JDK1.7及其之前

	Segment+Bin+Entry链表，进行插入和写入操作
	- Entry链表的读写机制类似于CopyOnWrite容器。数据类型的key、next声明为volatile变量，put操作是在每一个链表的链头插入；remove操作不修改源链表的而仅修改指针
	- 写操作时会对当前桶所对应的链表加锁（实际实现中是对链表的第一个节点加锁，为了不浪费空间去维护一个锁），但是这个锁不影响读操作，只影响其他写操作。特别的是，当一个桶的链表不存在时，该类是通过CAS的方式插入第一个节点的。
	- 弱一致性

	详情见[Here](https://blog.csdn.net/justloveyou_/article/details/78653929)的第31项

- JDK1.8及其以后

	基本结构和Map相同，但是table本身（Node数组）和table内的Node的val、next都是volatile修饰的；除此以外，设置table对象是通过compareAndSwap方法实现的，这保证了线程安全性。

	put过程中会如果对应桶的头节点不存在，则通过CAS方法写入如节点；否则锁住该头节点，然后插入，这个锁也只对其他写操作有效，而不锁定读操作。

	resize过程（ConcurrentHashMap中称为Transfer）：resize思想同HashMap，但是会加锁并将节点状态标记为resize中，此时如果有其他线程访问到该节点，则会帮助resize。

	获取size过程：维护了一个counterCells数组，用和LongAdder相同的逻辑（Cells），降低并发冲突。

### HashMap和ConcurrentHashMap有什么不同？

- 线程不安全/线程安全
- 获取size的方法不同
- 扩容方法不同

### CopyOnWrite容器是什么？如何实现的？什么叫弱一致性？为什么读不需要加锁？适用场景？

### 快速失败`fail-fast`和安全失败`fail-safe`？

### ThreadLocal是什么？怎么实现的？为什么会导致内存泄漏？

多个线程访问同一个对象时，如果希望每个线程使用对象成员的一个独占副本，可以将通过ThreadLocal达成这个目的。

其实ThreadLocal只是一个管理类，真正具体的内容存放的位置是线程实例的`threadLocals`对象里，也就是`thread.threadLocals`，其类型为`ThreadLocal.ThreadLocalMap`。所以说，数据是存在线程对象里的，而非ThreadLocal对象。`ThreadLocalMap`的`Entry`的key是ThreadLocal对象，value是对应线程存储的副本。也就是说，ThreadLocal成员变量仅仅是一个查询线程变量副本的一个标志，此处引用是弱引用，保证ThreadLocal机制不会影响到这个标志的回收。

![](.java面试相关.asset/2019-03-15-01-16-25-java面试相关.png)

但是这样会产生内存泄漏的问题，如果key指向的ThreadLocal对象已经回收，但是线程一直存在，这就导致了`thread->map->entry(value)->Object`的对象存在一串强引用关系而无法回收这个已经无法访问的对象，造成内存泄漏。比较好的解决方法是在使用完后手动移除这个value指向的对象。

### volatile 的两个特性？底层实现（具体到x86汇编）？

volatile有两个特性：

- 立即可见
- 禁止指令重排序

在 JVM 层面，禁止指令重排序就是一项 happens-before 原则的具体体现，通过内存屏障实现。happens-before 规定：对 volatile 变量的写操作一定在读操作之前。

在 x86 层面，JIT 会在编译 Java 字节码时，如果检测到了变量有 volatile 修饰，则会在写操作前添加 lock 指令（体系结构级别的内存屏障），这个指令有如下两个作用（其实 JVM 做的事情和 CPU 是相同的）：

- 将当前缓存的值立刻写回主内存
- 使所有 CPU 里其他涉及到该内存地址的缓存无效

参考[聊聊并发（一）——深入分析 Volatile 的实现原理](https://infoq.cn/article/ftf-java-volatile)

### 如何中断线程？interrupt做了什么？

## JVM

### JVM内存模型？

整体JVM有一个共用的主内存，每一个线程有自己的工作内存（对应CPU Cache）。内存在读写数据前必须将主内存中的数据读取到工作内存才能进行下一步操作。

这期间有八个基本操作分别用来读取、写入、加解锁变量：

- 读取：read/load/use
- 写入：assign/store/write
- 加锁：lock/unlock

### JVM内存分区？

五个主要部分：程序计数器、虚拟机栈、本地方法栈、方法区、堆区；堆区在具体实现的时候又会分为老年代、新生代，以实现不同的垃圾回收算法。

### GC如何确定要被回收的对象？

通过引用计数法（难以解决循环引用的问题，但是快）和可达性分析（通过分析`GC Roots`的引用链确定需要回收的对象，缺点是运行效率差一些，但是准确），JVM实现一般使用可达性分析的方法。

### 有哪些`GC Roots`？

作为GC Roots的对象的生命周期都是与他们所属线程（栈帧中的对象）、类的生命周期（类的静态成员）相同的：

- 虚拟机栈帧中本地变量表引用的对象
- 本地方法栈中Native方法引用的对象
- 方法区中类静态属性引用的对象
- 方法区中常量引用的对象

### GC是如何回收空间（三种）？

- 标记清除：会留下外部碎片
- 复制算法：用于新生代。过程是将内存分成三部分，每次都是将存活对象从其中两块（一块Eden和一块Survivor）复制到另外一块（Survivor）。
- 标记整理：就是标记清除，然后把内存规整，消去碎片。好处是不会产生碎片，缺点是停顿时间更长

### 何时会触发GC？

- `Young GC`：小对象分配在新生代但内存不足时，触发`Young GC`
- `Full GC`：`Minor GC`前，VM需要先检查老年代剩余空间大小是否足够容纳新生代所有对象，这是为了避免极端情况下所有对象都不能被回收且另一个Survivor区不足以容纳所有对象，而需要经过空间分配担保使新生代对象直接进入老年代的情况，如果不足则触发`Full GC`

### 有哪些垃圾回收器？

最基本的是Serial分别工作在新生代和老年代的两个版本，`Serial`/`Serial Old`都会完全`stop the world`然后单线程进行GC再恢复用户线程

随后的改进版本主要是将GC过程并行化的Parallel，也有分别工作在新生代和老年代的两个版本。`ParNew`/`Parallel Old`也会完全`stop the world`，他们和Serial GC区别就是将GC过程变成多线程的。虽然叫Parallel，但并不是只得和用户线程并行，而只是自身的多个GC线程并行。

除了上述四种外，还有CMS和G1收集器，CMS工作在老年代，需要配合其他的新生代垃圾回收器；G1同时工作在新生代和老年代。

### 介绍下CMS？

CMS是一个工作在**老年代**的垃圾收集器，分为如下四个阶段进行垃圾回收：

- 初始标记，`STW`

	标记`GC Roots`及其直接引用的对象

- 并发标记，并发

	与用户线程并行，遍历引用树

- 重新标记，`STW`

	修正并发标记过程中出现引用关系变动的对象的标记

- 并发清理，并发

	使用标记清除的方法进行垃圾回收。

优点：

- 并行的GC可以减少应用停顿

缺点：

- 对CPU资源敏感，如果本身CPU资源不充裕，反而会增加停顿
- 使用标记清除算法，会产生外部碎片，提高分配内存消耗。如果清理后仍然没有足够空间分配对象，则会临时调用`Serial Old`进行标记整理，产生更大的GC延迟

### 介绍下G1？比CMS强在哪？

G1取意`Garbage First`，G1在传统GC上做了很大的改进，主要是为了降低停顿时间，这些主要改动如下：

- 同时工作在新生代和老年代，但是他们在物理上不再是隔离的了
- 将堆区切分为块`Region`，整体基于标记整理算法，块之间采取复制算法
- 维护了一个由垃圾占比组成的`Region`列表，可以根据设定的最大`STW`时间优先回收垃圾占比的多的`Region`
- 每个`Region`维护`Rembered Set`保存引用自己的对象所在的`Region`，并将Region内的对象直接加入`GC Roots`减少扫描时间

相比CMS的改进主要有如下几点:

- 同时支持新生代和老年代的垃圾回收
- 可以设置最大停顿时间，在固定的时间内回收尽可能多的对象
- 基于 compact 算法可以避免产生外部碎片

回收过程和CMS类似，分别为`初始标记（阻塞）->并发标记（并发）->重新标记（阻塞）->垃圾回收（阻塞）`

### 为什么区分新生代老年代？

新生代对象的生命周期都很短，需要频繁GC，但老年代不需要频繁GC。对不同区域的对象使用不同的收集器可以提升JVM执行效率。

### 为什么Eden和Survivor的比例是8:1？为什么有两块Survivor？

很多实验表明新生代的每次GC只会有不到10%大小的对象存活，所以留出这部分空间即可，这是一个经验值。

两块Survivor可以只复制小部分数据，一是可以通过直接修改指针的方法避免清除数据的操作消耗；二是可以通过复制的方法避免内存中产生外部碎片。

### 什么时候对象会进入老年代？

- 大对象直接分配，可以参数设定

- 分配担保

- 年龄达到15的阈值

- Survivor中的相同年龄对象占Survivor一半空间以上

### 类加载流程？

![](.java面试相关.asset/2019-03-16-14-56-05-java面试相关.png)

1. 加载：通过类全全限定名加载二进制字节流，不仅限于文件
2. 验证：验证版本、安全性
3. 准备：JVM分配方法去的内存空间并初始化类变量初始值，此处只对类变量（static）进行操作。需要注意的是这里的初始值只是0值，真正在代码中设置的值需要在初始化步骤中执行`<clinit>`
4. 解析：将类文件中的引用从全限定名解析成真正的在JVM中对应的引用
5. 初始化：执行类初始化方法`<clinit>`，`<clinit>`方法是编译器收集类中的类变量赋值和静态语句块中的语句合并生成的。JVM保证这个方法的调用时线程安全的

### 双亲委派模型？为什么需要双亲委派模型？怎么破坏双亲委派模型？为什么需要破坏？

JVM中的类加载器层级如下：启动类加载器(`<JAVA_HOME>/lib`)<-扩展类加载器(`<JAVA_HOME>/lib/ext`)<-应用程序类加载器(默认类加载器，`<CLASSPATH>`)<-自定义类加载器。

双亲委派模型指的是除了启动类加载器，其他所有类加载器都会先将自己的加载请求委托给符加载器，如果父加载器加载失败，才会尝试自己加载。

为什么需要？双亲委派模型的意义在于避免自行实现的类覆盖了核心类库而导致功能异常，或是防止被恶意修改的类影响JVM运行安全。

怎么破坏？通过自行实现ClassLoader直接加载类而不先委托给父加载器。

为什么需要破坏？

# 算法与数据结构

## 快排、堆排、归并、希尔、桶排序，实现？复杂度？最差情况？优势、适用场景？

除了桶排序以外都为比较算法。

|算法|稳定性|时间复杂度|最好情况时间复杂度|最坏情况时间复杂度|空间复杂度|
|---|---|---|---|---|---|
|冒泡排序|稳定|$O(n^2)$|-|-|$O(1)$|
|快速排序|不稳定|$O(n \lg n)$|$O(n)$|$O(n^2)$|$O(1)$|
|堆排序|不稳定|$O(n \lg n)$|$O(n)$|$O(n^2)$|$O(n)$|
|归并排序|稳定|$O(n \lg n)$|-|-|$O(n)$|
|桶排序|稳定|$O(n)$|-|-|$O(n)$|

## 二叉树->二叉搜索树->平衡二叉搜索树->红黑树->B树->B+树，这个递进过程中的所有解决的问题和改进方法？

二叉树中数值是无序的，查找需要遍历整棵树。

二叉搜索树保证了$O(\lg n)$时间复杂度的查找。

但是二叉搜索树在极端情况下会转化成链表，查找速度降为$O(n^2)$，平衡二叉搜索树通过旋转的操作保证左右子树高度差不超过1，也就保证了查找时间复杂度始终控制在$O(n \lg n)$。

平衡二叉搜索树虽然保证了平衡，但是维持平衡的旋转操作代价有可能很高。红黑树是平衡二叉搜索树的简化实现，它并不严格要求左右子树高度差不超过1。这将旋转操作降低至最多三次，提高了效率。红黑树的统计性能强于平衡二叉搜索树，它通过**变色**和**旋转**两个操作维持如下的这些性质：

- 每个节点要么是红色，要么是黑色； 
- 根节点永远是黑色的；
- 所有的叶节点都是空节点（即 null），并且是黑色的； 
- 每个红色节点的两个子节点都是黑色（从每个叶子到根的路径上不会有两个连续的红色节点）； 
- **从任一节点到其子树中每个叶子节点的路径都包含相同数量的黑色节点**。该性质保证了最长路径不会超过任何其他路径的两倍

B树是应对红黑树这类二叉树在大数据量的情况下深度太大的问题而创造的**多路查找树**。通过允许每个节点下可能有更多的子节点以减小树深度，降低查找的统计时间。

B+树是优化了的B树，他的所有数据都存放于叶节点，这意味着连续的数据都是存放在相近的位置的，利于磁盘这类存储器的顺序访问。除此以外，B+树还将叶节点串联起来，这样扫描全表的操作就等同于链表遍历，而B树需要搜索整棵树。

## 90G文件，10G内存，怎么排序？

外排序。

1. 先将90G文件分为10G的块，每块加载到内存中快排后写回。

2. 将内存分为10块，取每个有序块中最小的1G加载到内存，剩余1G做归并缓冲区

3. 在内存中进行归并排序，写入归并缓冲区，如果归并缓冲区满了则写出到磁盘，随后清空归并缓冲区；如果某个数据块都读完了，则按序到磁盘中读取新的数据块到内存中，重复上述过程直到所有磁盘中的数据都已排序完毕

# 计算机网络

## 四层模型、七层模型每一层是什么？做了什么？应用？

下面这张图需要注意的几个协议：

- 应用层

	HTTP(80)、FTP(20数据端口，21控制端口)、SSH(22)、HTTPS(443)

- 会话层

	SSL/LDAP

- 网络层

	**ARP**属于网络层协议，下图有错。仔细想想ARP的作用是通过IP获取MAC地址，不可能只工作在数据链路层。

	IP ICMP IGMP

![](.java面试相关.asset/2019-02-23-21-27-23-java面试相关.png)

## TCP/UDP区别？

|TCP|UDP|
|---|---|
| 面向连接 | 无连接 |
| 可靠传输 | 不可靠传输 |
| 拥塞控制 | 无 |
| 面向字节流 | 面向报文 |
| 只支持点对点 | 支持一对一、一对多、多对一、多对多 |
| 20首部字节 | 8首部字节 |

## TCP如何实现可靠传输？

校验和、确认机制、超时重传的方式实现可靠传输。

## TCP如何实现拥塞控制？

通过滑动窗口的机制，通过动态发送窗口的大小进而控制发送速率的**TCP拥塞控制算法**，发送窗口又是由本地的拥塞窗口和接收端的接收窗口中的较小值决定的。发送过程中，拥塞窗口大小会根据ACK报文的接收情况发生变化，具体涉及到算法有**慢启动、拥塞控制、乘法减小、快速恢复**。

![](.java面试相关.asset/2019-03-12-18-11-46-java面试相关.png)

- 慢启动

	每接收到一个ACK，拥塞窗口大小加1，即每个RTT（报文往返时间，Round Trip Time）拥塞窗口翻倍。

- 拥塞避免

	当拥塞窗口大小超过指定阈值时，每个RTT拥塞窗口只加1。

- 乘法减小

	当出现报文超时，阈值减小到原来的一半，拥塞窗口重置为1，重新开始慢启动过程。

- 快速恢复

	当收到连续三个重复ACK时（不同于超时），不等待超时，直接**快重传**这个丢失的报文，然后将阈值减半，拥塞窗口大小重置为减半后的阈值大小，随后执行拥塞避免算法。

	这里之所以不重新慢启动，是因为如果可以连续收到三个ACK，表明网络情况并不是很差，而只是意外丢失了一个报文段，所以无需重置拥塞窗口而只要稍微限制发送即可。

## TCP三次握手过程？为什么需要三次握手？

![](.java面试相关.asset/2019-02-26-19-06-04-java面试相关.png)

三次握手是为了交换并确认彼此已知对方的初始序列号，完整的建立全双工过程的两端，三次握手的1、2步建立了server到client的通道，2、3步建立了client到server的通道。
	
如果不存在第三次ACK，那么服务端B并不能确定发送端A已经知道了B的初始序列号，此时有两种情况：

- 如果发送端A没有收到第二次握手报文：A根本无法发送数据，因为A不知道B的初始报文序号

- 如果发送端A收到了第二次握手的报文：第二次握手到第一个数据报文这段时间内，服务端B并不确定发送端A已经获知自己(B)的初始报文序号，只能不停的重发SYN报文，这会造成大量的资源浪费。

所以，无论如何一定要三次握手才能保证连接的顺利进行。

如果A发给B的确认丢了，该如何？A会超时重传这个ACK吗？不会！TCP不会为没有数据的ACK超时重传。

那该如何是好？B如果没有收到A的ACK，会超时重传自己的SYN同步信号，一直到收到A的ACK为止。

三次握手的情况下，如果服务器端接到了客户端发的SYN后回了SYN-ACK后客户端掉线了，服务器端没有收到客户端回来的ACK，那么，这个连接处于一个中间状态，即没成功，也没失败。于是，服务器端如果在一定时间内没有收到的TCP会重发SYN-ACK。在Linux下，默认重试次数为5次，重试的间隔时间从1s开始每次都翻倍，5次的重试时间间隔为1s, 2s, 4s, 8s, 16s，总共31s，第5次发出后还要等32s才知道第5次也超时了，所以，总共需要 1s + 2s + 4s+ 8s+ 16s + 32s = 63s，TCP才会断开这个连接。

上述解释参考

- [TCP 为什么是三次握手，而不是两次或四次？ - 车小胖谈网络](https://mp.weixin.qq.com/s/NIjxgx4NPn7FC4PfkHBAAQ)

- [传输控制协议 - WiKi](https://zh.wikipedia.org/wiki/%E4%BC%A0%E8%BE%93%E6%8E%A7%E5%88%B6%E5%8D%8F%E8%AE%AE)

## TCP四次挥手过程？为什么需要四次挥手？最后的等待确认时间为什么是2*MSL(Maximum Segment Lifetime)？

![](.java面试相关.asset/2019-02-27-14-31-22-java面试相关.png)

四次挥手的原因和三次握手基本相同，每两次操作断开了全双工中一方的通道，由于连接建立过程中没有数据传输，所以可以合并为三步，但是客户端断开连接时服务端可能有数据尚未传输完，所以必须需要四步才能完整的断开连接。如若没有最后一个客户端发送的ACK，服务端直接关闭，那么这条最终的FIN报文并不能确定传输到客户端，客户端也就不能正常关闭该连接，这会影响到后续新连接的建立，服务端不能确定之后的报文是新的请求还是来源于上一个已关闭的连接的报文。

最后2*MSL等待时间是为了客户端确认自己的断开连接确认报文送达了，如果客户端的ACK未送达，则服务端会重发FIN报文。这个时间时考虑到了服务端没有收到ACK报文因而重发FIN报文的时间（1 MSL）加上ACK到达服务端的最长时间（1 MSL）。

## TIME_WAIT是什么？为什么要有TIME_WAIT？TIME_WAIT状态的连接过多会怎么样？怎么解决TIME_WAIT过多的问题？

TIME_WAIT：断开连接的**客户端**在返回最后的ACK报文后，需要等待一段固定时间再关闭连接，这段时间客户端就会处于TIME_WAIT的状态。

存在的意义？本质上是为了正常断开连接，目的有二：

1. 释放资源（端口、内存等，主要是服务端的），否则新的SYN报文会被拒绝
	
2. 确认连接真的断开了，防止上一个连接延迟在网络中的报文对下一个连接产生影响（虽然概率不大）

过多会怎样？大量端口资源被占用，从而导致新连接无端口可用，请求并发数收到很大限制。

解决方案？手动设置中降低TIME_WAIT的时间；或者开启TIME_WAIT状态的端口重用或快速回收，但是这样好像会导致一些[未知的问题](https://www.cnxct.com/coping-with-the-tcp-time_wait-state-on-busy-linux-servers-in-chinese-and-dont-enable-tcp_tw_recycle/)。

## CLOSE_WAIT是什么？为什么可能出现很多CLOSE_WAIT状态？CLOSE_WAIT状态的连接过多会怎么样？怎么解决CLOSE_WAIT过多的问题？

CLOSE_WAIT：**服务端**在接收到发送方的FIN报文后，如果目前还有数据没有发送完成，则会选择继续发送，在这段时间内，服务端的状态为CLOSE_WAIT，发送完后服务端会发送FIN报文，随后等待客户端ACK报文，进入LASK_ACK状态。

为什么会出现很多CLOSE_WAIT？如果程序中没有**手动关闭socket**，那么服务端并不会发出FIN，此时TCP连接需要被动地等到超时才能断开连接，此时持有的资源也不会释放。

过多会怎样？大量的资源（端口、内存等）被占用，导致新连接无法建立。

怎么解决？编程时注意手动关闭socket。

## GET/POST异同？

语义不同：GET一般为请求资源；POST为更新资源

参数不同：GET的参数在URL上；POST的参数既可以在URL上，也可以在请求体中

安全方面：因为参数位置的缘故，POST请求安全性相对来说好一些

长度限制：GET受限于最大URL长度；POST无限制

## 浏览器输入域名到返回页面发生了什么？

1. DNS查询指定URL，如果本地有缓存则使用本地缓存，如果无缓存则查询DNS服务器。

2. DNS服务器检查本地是否有这个域名对应的IP，如果有则返回；如果没有则递归或迭代查询上层DNS服务器

3. 建立TCP链接，封装HTTP请求和TCP报文，发送至服务器（一般是负载均衡服务器）

4. 反向代理服务器将请求打到对应的服务器

5. 服务端根据业务逻辑获取对应资源，返回数据，如果是后端渲染则返回渲染后的页面，如果是前后端分离的一般是序列化后的数据

6. 浏览器解析HTML，构建DOM树，对遇到的每一个资源再次执行上述过程

7. 如果前端还有渲染行为，则进行前端渲染，随后结束

底层可能还有其他操作：

- ARP寻找下一跳硬件地址，在链路中传递
- NAT转发
- CDN缓存
- HTTPS

## 网络层协议ARP的工作流程？

ARP是一个根据IP地址获取物理地址的协议。流程如下：

1. 首先源主机会检查本地ARP缓存是否已有数据，如果有则直接使用；如果没有，继续
2. 在自己所在的网段发起一个ARP请求的广播，ARP请求包含了源主机的IP地址、硬件地址和目标主机的IP地址
3. 收到该广播的主机会检查这个目标IP和自身是否相同，如果不同则丢弃；如果相同，目标主机进行下一步
4. 目标主机首先将源主机的IP和硬件地址加入到自己的缓存中，然后发送ARP返回报文
5. 源主机收到返回报文，将目标主机的IP和硬件地址加入到缓存，并且使用此地址发送信息；如果源主机始终没有收到返回报文，则查询失败。

## Cookie和Session的异同

关系上来讲，Session一般依赖于客户端存储在Cookie中的数据实现；如果Cookie被禁用了，可以通过URL回传SessionId实现。Cookie和Session的对比如下：

||Cookie|Session|
|---|---|---|
|存储位置|客户端|服务器|
|限制|个数、容量受限于浏览器|无限制|
|安全性|相对较差|安全|
|消耗|对服务器无消耗|Session过多会增大服务器负载|

## 跨站脚本攻击(Xss, Cross Site Scripting)

通过在正常页面嵌入攻击者的脚本实现攻击，例如用户输入的内容包含了HTML标签和JS代码。防范方法是对用户提交的内容过滤，永远不要信任用户的输入。

## HTTP常见状态码？200/201/202/204/206/301/302/304/400/401/403/404/500？

- 200 OK：正常成功
- 201 Created：请求创建的资源已创建
- 202 Accepted：请求已接受但还未被处理，请求的操作可能并不会成功
- 204 No Content：服务器正常处理了请求，且不需要返回信息
- 206 Partial Content：服务端返回了部分数据，用于断点续传一类的需求
- 301 Moved Permanently：永久重定向
- 302 Moved Temporarily：临时重定向
- 304 Not Modified：服务器表示Get请求的对象从上次请求没有改变，可以使用缓存
- 400 Bad Request：请求错误，语义或参数
- 401 Unauthorized：请求需要验证身份才能继续执行
- 403 Forbidden：服务器拒绝执行请求，与401不同的是验证信息并不能对这个请求提供帮助
- 404 Not Found
- 500 Internal Server Error：服务器执行时发生了异常

## HTTP1.1/2.0各自改进了什么？

HTTP1.1的主要改进:

- 长连接：避免了TCP握手的时间开销
- 强制要求的HOST请求头字段，应对一台服务器（一个IP地址）部署了多个域名服务的情况，服务器需要通过HOST字段区分服务
- 更丰富的缓存策略：HTTP1.0只支持`If-Modified-Since`
- 100的临时状态码：表示服务器已经收到了请求的一部分，正在接受其他部分

HTTP2.0三个主要特性：

- 多路复用：多个请求可以连续发送而不必一个一个等待返回，提高了并发度，通过将请求的信息封装为二进制帧实现。
- 头部压缩：省略公共字段、字符串使用**huffman编码**
- 服务端推送：服务端提前推送客户端可能使用到的数据，预先缓存

服务端推送在HTTP1.1时期有一些历史实现：

- 延迟应答

	服务端接收到请求后不立即应答，而是再需要推送消息时候再应答，这是一个妥协的服务端推送方案

- WebSocket

	HTTP通信过程中使用UPGRADE首部字段将通信协议切换到WebSocket，之后使用WebSocket通信

## HTTP长连接短连接？

核心区别在于HTTP连接是不是共享同一个TCP连接。

短连接：http1.0中默认每个http请求会开启一个TCP连接，适合请求不频繁的

- 优点：节约服务器资源

- 缺点：单次请求的延时过长

- 适用于数据传输不频繁的场景

长连接：http1.1中默认每个页面共享一个TCP连接，在http首部设置connection为keep-alive。如果长时间未有新的http报文，也可能会被关闭，这个等待时间取决于设置的超时时间

- 优点：因为节约了握手的时间开销，传输内容延时短。

- 缺点：服务器为了保持连接，所以压力比较大

- 适用于需要频繁数据传输且用户量不大的情况

## 对称加密和与非对称加密？

对称加密只存在一把密钥同时用于加密和解密。

非对称加密会有两把密钥，分别为私钥和公钥，使用一个密钥加密后必须使用另一个密钥解密。

## 介绍HTTPS？

HTTPS是HTTP+SSL，SSL工作在会话层，HTTPS会先使用SSL建立安全的会话链路，再在该链路上进行通信。HTTPS先使用非对称加密，后使用对称加密。对称加密的密钥通过非对称加密的方式在握手阶段传输。

## 为什么HTTPS使用混合加密？

因为单一的非对称加密是依赖于证书的静态密钥，仍有被破解的可能，而HTTPS对称加密阶段的密钥是根据客户端与服务器报文中的随机数生成的，这种短时间有效的密钥大大加强了这次通信过程中的安全性。

## HTTPS握手过程？如何保证安全性？

![](.java面试相关.asset/2019-03-20-18-51-02-java面试相关.png)

总体来说，HTTPS通过非对称加密的方式传输对称加密过程所需的密钥。并且通过在应用层报文附加报文摘要以防止报文被篡改。

# 操作系统

## 进程、线程

### 进程和线程是什么？

- 进程时程序的运行时，是系统分配资源的基本单位
- 线程是进程内的子任务，是系统分配时间片的基本单位

### 进程、线程区别？

- 线程依赖于进程存在而存在

- 进程是资源分配的最小单位；线程是时间片调度的最小单位

- 进程独享内存空间；线程没有自己的内存空间，而是共享进程的内存空间

### 进程间通信方式（IPC，Inter-Process Communication）

- 文件

- 管道
	
	- 匿名管道：FIFO，半双工；生命周期仅限于创建该管道的进程，多用于父子进程间通信
	- 命名管道：FIFO，利用文件系统实现；生命周期可与操作系统一样；可用于无亲缘关系的进程通信

- 信号

	一种异步的通知机制。当一个信号发送给一个进程，操作系统中断了进程正常的控制流程，此时，任何非原子操作都将被中断。如果进程定义了信号的处理函数，那么它将被执行，否则就执行默认的处理函数。

- 消息队列

- 共享内存

- 套接字

- 信号量

	其实信号量并不是进程通信手段，而是线程间用于实现锁的同步控制方法

参考自[WIKI_IPC](https://zh.wikipedia.org/wiki/%E8%A1%8C%E7%A8%8B%E9%96%93%E9%80%9A%E8%A8%8A)

### 进程调度策略？

- FCFS 先来先服务，非抢占

- SJF 最短作业优先

- 时间片轮转 可抢占也可不抢占

- 优先级调度（抢占、非抢占）

- 优先级、时间片、抢占式的组合

### 进程间同步方式？

- 信号量和PV操作

	通过信号量和PV操作来协调线程的同步。信号量取值范围只有0和1的时候就是互斥锁。

- 管程（或称作为监视器，Monitor）

	管程是为了解决大量信号量和PV操作散布不易管理的问题而提出的。

	管程包含了临界资源和对这些资源操作的一组过程。这些临界资源只能通过管程中的过程访问而不能直接访问。且同一时间管程中只会有一个进程在执行。

	Java已经通过Object对象上的wait/notify/notifyAll三个方法实现了管程。

## 死锁

### 什么是死锁？

在两个或更多线程并发中，每个线程都等待着其他线程持有的资源并且不能放弃自己已经持有的资源，导致系统状态不能继续向前推荐，这种状态称为死锁。

### 死锁产生的条件？

- 资源互斥
- 请求与保持
- 循环等待
- 不剥夺

### 怎么处理死锁？

通常从四个角度处理死锁:

- 死锁预防

	使系统在构建之处就破坏死锁产生的条件进行预防。

- 死锁避免

	在资源分配过程中，防止系统进入不安全状态。所谓不安全状态指的是找不到一个分配资源的序列使得所有每个线程都能顺利完成。

	使用**银行家算法**。

- 死锁检测

	运行时出现死锁，能及时发现死锁。

	使用**资源分配图化简**。

- 死锁解除

	发生死锁后，操作系统可以撤销进程，回收资源，使系统解除死锁状态。

### 银行家算法是什么？

银行家算法指的是系统会持有所有的资源，每个进程都要向操作系统告知自己**需要的资源的最大量**、**每次申请需要的申请量**，并且保证在**有限时间内归还资源**。操作系统会在进程申请资源时检查剩余的资源和进程已经持有的资源是不是满足了进程需要资源的最大值，如果满足，证明可以分配给该进程资源且进程可以安全执行完成并返回这些资源；如果不满足则拒绝分配资源。这个算法使系统避免了进入不安全状态。

### 资源分配图是什么？

资源分配图代表着进程对资源的请求情况，如果资源分配图化简后有边剩余，则代表系统中出现了死锁。用于死锁检测。参考[操作系统原理_北京大学_Coursera](https://www.coursera.org/lecture/os-pku/zi-yuan-fen-pei-tu-JvKcU)。

InnoDB就是通过类似的等待图实现的死锁检测，如果发现了死锁就会把涉及到的事务中权重最低的一个回滚。

## 虚存、页式存储

### 虚拟内存的寻址过程？

（虚拟地址）->TLB->页表->（物理地址）->Cache->内存

### 什么是缺页中断？页面置换算法？抖动是什么？

缺页中断指的是应用访问的虚存对应的实页并不在内存中的情况，此时需要页面置将该虚页置换到内存中。

页面置换算法有FIFO、LRU、OPT。

抖动指的是虚页频繁换进换出，一般出现在实际内存空间不足的情况。

## Linux

### 常用指令，参考Linux章节

# 设计模式

## 设计模式的七大设计原则？

## 单例模式？Java的几种写法？

- 直接加载

	```java
	public class Singleton{
		private static Object singleton = new Object();
		public static synchronized Object getSingleton(){
			return singleton;
		} 
	}
	```

	- 优点：写法简单粗暴
	- 缺点：每次读都要加锁，浪费时间；并且直接创建了对象，浪费空间

- 懒加载

	```java
	public class Singleton{
		private static Object singleton = null;
		public static synchronized Object getSingleton(){
			if (singleton == null){
				singleton = new Object();
			}
			return singleton;
		}
	}
	```

	- 优点：实现简单；懒加载，节约资源
	- 缺点：每次获取单例对象都要加锁，效率低

- 双重检查

	```java
	public class Singleton{
		private static volatile Object singleton = null;
		public static Object getObject(){
			if (singleton == null){
				synchronized (Singleton.class){
					if (singleton == null){
						singleton = new Object();
					}
				}
			}
			return singleton;
		} 
	}
	```

	优点：懒加载；只在初始化的时候才加锁
	缺点：每次都要进行先判断

- 静态内部类

	```java
	public class Singleton{
		//private构造方法，避免构造多个单例对象
		private Singleton(){}

		public static Object getSingleton(){
			return Holder.singleton;
		}

		class Holder{
			public static final Object singleton = new Object();
		}		
	}
	```

	优点：实现简单、依赖于JVM实现懒加载效率高

- enum

	```java
	public enum Singleton{
		HOLDER();

		public Object singleton = new singleton();
		Singleton(){}
		public Object getSingleton(){
			return this.singleton;
		}
	}
	```

## 为什么上面的双重检查的方法要使用volatile关键字？

JVM创建对象分为三个阶段：

1.栈上分配引用空间、堆上分配内存空间
2.初始化对象
3.将引用指向堆上对象

后两步可能会被重排序，那么假若此时有线程B对调用Singleton.getInstance方法，那么在第一次检查时会认为引用已经存在并返回了这个引用，但是此时返回的引用指向的对象还未完全初始化，例如某些字段值还未初始化完毕。这样的对象处于不确定的状态，此时如果调用这个不确定状态的对象则会抛出异常。

但双重检查已经是一种过时的方法（Java 并发编程实践在 2006 年、甚至更早就提到了），主要因为以下两个原因：

1. JVM 低竞争状态下的同步消耗已经由于 JVM 1.5 版本后的优化降低很多，不需要应用层考虑太多争用同步消耗的问题
2. 语义上，双重检查锁定比内部类不易于理解。

## 为什么使用内部类静态变量可以实现单例模式？

1. JVM会保证类的静态变量只初始化一次，这保证了安全。

2. 内部类会在外部类第一次使用到它的时候才会被加载，这保证了懒加载。

## 为什么使用枚举类型可以实现单例模式

枚举类型实际上就是Enum类的子类，每一个枚举变量都是该类的一个静态对象，静态对象会由JVM保证只初始化一次。

## 如何破坏单例模式？

通过反射的方法获取到单例类的构造器方法，通过newInstance实例化一个对象出来。这个对象和原本的单例对象就不是同一个对象了，单例模式也就被破坏了。

## 观察者模式

## 装饰器模式

# Spring相关

# MySQL

## 基本的SQL语句，CRUD？设置索引？

- create

	```sql
	CREATE TABLE `media_birth_selected_0` (
		`id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '自增id',
		`album_id` bigint(20) NOT NULL COMMENT '宝宝相册ID',
		`media_id` bigint(20) NOT NULL COMMENT '照片ID',
		`score` double NOT NULL COMMENT '照片美学评分',
		`special_day_type` int(8) NOT NULL COMMENT '添加的日期类型:枚举->数值',
		`date_time` int(11) NOT NULL COMMENT '照片拍摄时间',
		`status` tinyint(4) NOT NULL COMMENT '状态：当前使用/可用/已被更换/源照片被删除',
		`rank` int(8) NOT NULL COMMENT '该条记录在[纪念日|节日|其他]中的级别',

		PRIMARY KEY (`id`),
		UNIQUE KEY `uk_mbs_id` (`album_id`, `media_id`),
		KEY `idx_mbs_tag` (`album_id`, `tag`),
		KEY `idx_mbs_rank` (`album_id`, `rank`, `date_time`, `score`),
		KEY `idx_mbs_status` (`album_id`, `status`, `date_time`, `score`)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8;
	```

- insert

	```sql
	insert into t (col1, col2) values (1, 2);
	```

- update

	```sql
	update t set col1 = col1 + 1 where primary_key_id = 1234;
	```

- delete

	```sql
	delete from t where primary_key_id = 1234;
	```

## 三范式？

- 一范式：列是原子不可分的
- 二范式：有主键，保证完全依赖。每列都和主键相关，而不能只和主键的一部分相关
- 三范式：无传递依赖。每个列都是和主键直接相关的，而非间接相关

## MyISAM和InnoDB特点、区别、使用场景？

|功能|MyISAM|InnoDB|
|---|---|---|
|事务|不支持|支持|
|锁机制|表级锁|行级锁、间隙锁|
|外键|不支持|支持|
|适用场景|读多写少，不需要**事务**支持|需要**事务**支持|

## 创建索引的SQL语句？实现方法？

`create index on table(column);`

对某列建立有序的数据结构，提高查询的更新的效率，但是相应的会因为维护索引导致插入删除性能有所降低。

MySQL创建索引其实就是创建了一个有索引结构的新表，然后把旧表数据复制过去，效率比较低。表数据量大的时候谨慎使用这个方法。

InnoDB使用B+tree实现索引，其他存储引擎还有Hash方法实现的索引（内存引擎）、全文索引等。

## 什么是聚簇索引？

索引键值的逻辑顺序与索引所服务的表中相应行的物理顺序相同的索引，被该索引称为聚集索引，反之为非聚集索引。InnoDB的主键是聚簇索引的。

## 什么是覆盖索引？

当索引中的内容覆盖了一个查询要查找的所有列，称这个查询使用了覆盖索引。这种情况下，数据库不需要通过查找到的主键值进行二次查找，而是可以直接返回。

## 主键和唯一索引的区别？

主键是一个记录的唯一标识。一张表只能有一个主键。

唯一索引指的是其限定的列的取值不能出现冲突，但允许多个NULL。

主键和唯一索引区别主要有如下几点：

- 主键不允许NULL；唯一索引允许NULL，而且可以有多个NULL（在数据库中NULL和NULL不等，所以这不算破坏了唯一性）
- 一个表只能有一个主键而可以有多个唯一索引
- 从设计角度讲，主键用于唯一标志行，而唯一索引只是一种限定条件

## 哪些键适合建索引？

- 用于**查找**的建
- 用于**连接**的键
- 用于**排序**、**分组**的键

## 什么时候索引会失效？

- 以`%`开头的模糊匹配
- `OR`涉及到的列没有各自的索引
- 匹配的并非索引列本身，而是使用表达式、函数一类的计算值，例如`where col*2 <= 100`
- 要查询的数据量占据了全表的很大部分（20%以上）时，使用索引会带来大量的随机IO，此时MySQL会选择全表扫描的顺序IO
- 多个查询条件使用列未满足最左前缀索引的要求
- 范围匹配后的索引全部失效

## B树是什么？

B树是一棵多路平衡查找树，如果每个节点可以有最多m个子树，则其每个节点可以存储关键码数量的范围在`[m/2, m]`区间内，少于或超出会进行节点重组和拆分。这种设计保证了B树是平衡、数据有序的，并且树的高度相比二叉平衡树可以减少很多。

## B+树改进？这个索引结构为什么有效？

B+树其改进主要是如下几点：

- 所有的数据只存在于叶节点，非叶节点只存储关键码用于索引
- 叶节点是前后相连的，对应到全数据扫描的操作只需要遍历所有叶节点即可，而不用像B树一样深搜整棵树

B+树的一个叶节点一般被实现为一个页，页是在磁盘上一块连续的存储空间，这充分利用了磁盘I/O的顺序读取快而随机读取慢的特点。

## 什么是Hash索引？为什么InnoDB没有显式的Hash索引？

Hash索引是一个适用于随机访问的索引，不适合磁盘这类随机访问很慢但顺序访问较快的存储器，而是适合内存、固态硬盘这类存储器。

## 事务的四个基本特性？

- 原子性：事务要么全部执行，要么全部不执行

- 一致性：事务执行前后的数据库完整性不会被破坏，例如银行存款总数不会改变

- 隔离性：多个事务之间是隔离的。一个事务不会看到另一个事务执行时的中间状态的数据。

- 持久性：在事务完成后，事务对数据库所做的变更会永久写入数据库中，即使发生致命错误也不会丢失，且无法回滚

## 事务的隔离级别？

- Read Uncommitted：读未提交，可以读到其他事物的中间状态数据。产生“脏读”

- Read Committed：读提交，可在一个事务内多次读读到不同结果。避免“脏读”，产生“不可重复读”

- Repeatable read：可重复读。避免“脏读”、“不可重复读”，产生“幻读”

- Serializable：串行化，避免“脏读”、“不可重复读”、“幻读”

	对几种读问题的解释：

	- 脏读：会在一个事务内读到另外一个事务回滚之前修改的数据。如果出现脏读，那事务的隔离性已经没有得到保证

	- 不可重复读：会在一个事务内的多次读取同一数据时读出不同的结果

	- 幻读：事务A读取一些行时，事务B在该范围内插入了新行，事务A再次读取，结果与第一次读取不同，好像是幻觉一样，谓之“幻读”。

注意区分幻读与不可重复读，不可重复读对应**非结构性修改**，例如更新（update）；幻读对应**结构性修改**，例如插入（insert）与删除（delete）

## MVCC是什么？解决了什么问题？

## InnoDB插入数据时发生了什么？

## InnoDB如何保证事务持久性的？

## MySQL复制过程？

## drop、delete与truncate的区别

- drop会移除整张表，包括索引和权限。不能回滚，且不会触发任何DML触发器

- delete可以删除表中的任意多数据

	- 用户需要执行commite或rollback

	- delete会触发表上的触发器

- truncate清除表中所有数据但不移除表
	
	truncate要比同样目的的delete快一些，但**不能回滚**，不会触发表上触发器。

## 什么是视图？

视图是虚拟的表，具有物理表的功能，可以插入、删除、修改等，但不能设置索引。

## 什么是游标？

查询返回行集，通过改变游标的位置查看行集中的具体行信息。

## 什么是触发器？

在满足某种条件时执行特定的语句序列，多用于保持数据库的完整性。

# Redis

# 分布式系统